# Building flatpak for kazv

All blocks are run from the repository root unless specified.

Setup the environment:

```
cd packaging/GNU-Linux/flatpak
python -m venv venv
. ./venv/bin/activate
pip3 install aiohttp toml
wget -O./venv/bin/flatpak-cargo-generator https://raw.githubusercontent.com/flatpak/flatpak-builder-tools/master/cargo/flatpak-cargo-generator.py
chmod +x ./venv/bin/flatpak-cargo-generator
cd ../../..
```

Setup flatpak:

```
flatpak remote-add --user --if-not-exists flathub 'https://flathub.org/repo/flathub.flatpakrepo'
```

Fetch cargo sources (if you run into problems building vodozemac-bindings, run `git pull` in the `vodozemac-bindings` directory and rerun `flatpak-cargo-generator` command):

```
cd packaging/GNU-Linux/flatpak
. ./venv/bin/activate
git clone https://lily-is.land/kazv/vodozemac-bindings.git
flatpak-cargo-generator vodozemac-bindings/Cargo.lock -o cargo-sources.json
cd ../../..
```

Build:

```
cd packaging/GNU-Linux/flatpak
flatpak-builder --jobs=NJOBS --user --install-deps-from=flathub --force-clean --ccache --repo=repo flatpak-build moe.kazv.mxc.kazv.yaml
flatpak build-bundle repo kazv.flatpak --runtime-repo='https://flathub.org/repo/flathub.flatpakrepo' moe.kazv.mxc.kazv
```

Replace `NJOBS` with the number of parallel jobs you want to run.

Install:

```
cd packaging/GNU-Linux/flatpak
flatpak install --user --bundle ./kazv.flatpak
```

# See also

[Upstream documentation about building a flatpak](https://docs.flatpak.org/en/latest/hosting-a-repository.html)
