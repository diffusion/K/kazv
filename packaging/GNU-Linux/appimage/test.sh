#!/bin/bash

# inspired by https://github.com/johanhelsing/docker-qt-tests/blob/master/scripts/test-qt.sh

set -x

weston --backend=headless-backend.so --idle-time=0 &

sleep 5
QT_QPA_PLATFORM=wayland make test

ret=$?

echo "Tests returned $ret"
echo

cat Testing/Temporary/LastTest.log

kill %1

exit $ret
