# Description

kazv is a convergent matrix client and instant messaging app.

# License

Copyright (C) 2020-2023 the Kazv Project <https://lily-is.land/kazv>

kazv is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

kazv is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with kazv.  If not, see <https://www.gnu.org/licenses/>.

# Compiled executables

| Operating system | Architecture | Format   | Download links                                     |
|------------------|--------------|----------|----------------------------------------------------|
| GNU/Linux        | amd64        | AppImage | [Debug][gl-amd64-ai-d] [Production][gl-amd64-ai-p] |

[gl-amd64-ai-d]: https://lily.kazv.moe/kazv/kazv/-/jobs/artifacts/servant/download?job=build:GNU/Linux-amd64-debug:kf6
[gl-amd64-ai-p]: https://lily.kazv.moe/kazv/kazv/-/jobs/artifacts/servant/download?job=build:GNU/Linux-amd64-prod:kf6

# Installation

## GNU/ or musl/Linux

### flatpak

If you have flatpak, you can install kazv via flatpak. Because flatpak currently cannot be built in an unprivileged container, we are unfortunately unable to provide a built image as a part of CI/CD. See [this flatpak bug][flatpak-docker-bug] for more details. It seems that upstream is very uninterested in resolving this security issue, as this bug report has been open for more than five years with zero (0) comment from Collaborators of this repository.

[flatpak-docker-bug]: https://github.com/flatpak/flatpak/issues/3027

However, we will release *manually-built* flatpak packages from version 0.6.0 onwards. You can also build the flatpak via the instructions specified in the README file in the `packaging/GNU-Linux/flatpak` directory.

### Gentoo

If you are using Gentoo, you can use [tusooa-overlay][tusooa-overlay]
to install kazv.

[tusooa-overlay]: https://gitlab.com/tusooa/tusooa-overlay

### Nix/NixOS

Stable kazv is available as a [part of nixpkgs][nixpkgs].
Development version is available in [XYenon's nur-packages repository][xyenon-repo].

[nixpkgs]: https://github.com/NixOS/nixpkgs/blob/master/pkgs/by-name/ka/kazv/package.nix
[xyenon-repo]: https://github.com/XYenon/nur-packages

## Windows

You can build kazv using [Craft][craft]. You will need to use [our Craft blueprints][craft-blueprints-project-kazv]. Follow the instructions in that repository to build and run kazv. We also upload the binaries to the [release page][kazv-release].

[craft]: https://community.kde.org/Craft
[craft-blueprints-project-kazv]: https://lily-is.land/kazv/craft-blueprints-project-kazv
[kazv-release]: https://lily-is.land/kazv/kazv/-/releases

## Dependencies

- ECM
- Qt6: Core Gui Qml QuickControls2 Svg Concurrent Multimedia Network Widgets (ImageFormats is a recommended runtime dependency, without which formats like webp cannot be displayed) (Test and QuickTest if you want to build tests)
- KF6: Kirigami KConfig KIO Notifications CoreAddons KirigamiAddons (BreezeIcons is recommended)
- nlohmann_json
- libkazv
- cmark

## Build process

Normal build:

```
mkdir build && cd build
cmake ..
make install
```

Updating external JavaScript libraries (advanced, usually not needed):

You need to first have Node.js available.

```
cd src/js/Intl.js
npm install
cd ..
npm install
./transform.bash
```

Follow the instruction in [emoji.lisp](./tools/emoji.lisp) to update the emoji list (usually not needed)

# Contributing

## Dev chat

We use Matrix to communicate. Join the matrix room at [#kazv:tusooa.xyz](https://matrix.to/#/#kazv:tusooa.xyz).

## Bugs

Report bugs to [our repository on Lily Islands](https://lily-is.land/kazv/kazv) (chances are it is the page you are currently viewing), or email to [project-bugs@kazv.moe](mailto:project-bugs@kazv.moe).

Report security vulnerabilities using a confidential GitLab issue on Lily Islands, or email to [project-security@kazv.moe](mailto:project-security@kazv.moe).

## Write code

To contribute code to the Kazv Project, open a merge request on Lily Islands, or email your patch to [project-patch@kazv.moe](mailto:project-patch@kazv.moe).

## Translations

kazv uses [fluent](https://projectfluent.org) for translations. To add a new language, add it into `src/l10n/translations.qrc`, `src/l10n/config.json`, and populate translations in the files under `src/l10n/<language code>`.

Before a change can be landed, the UI has to be translated into all of the **core languages**. One who submits a patch only needs to do the translations for at **least one** core language. The rest can be translated by others.

The list of **core languages** is defined in `src/js/l10n-config.json`. If you want to add other languages into the list and you have enough commitment to keep the translations up-to-date (refer to the rule before), you can submit a patch to do so.

We have a CI job to validate all the translation files, and ensure translations in all of the core languages are up-to-date.

## Donate

You can now donate to the Kazv Project on Liberapay: <https://liberapay.com/theKazvProject/>.
