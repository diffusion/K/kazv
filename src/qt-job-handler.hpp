/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <memory>

#include <QObject>

#include <jobinterface.hpp>

using namespace Kazv;

class QtJobHandler : public QObject, public JobInterface
{
    Q_OBJECT

public:
    QtJobHandler(QObject *parent = 0);
    ~QtJobHandler() override;

    void async(std::function<void()> func) override;
    void setTimeout(std::function<void()> func, int ms,
        std::optional<std::string> timerId = std::nullopt) override;
    void setInterval(std::function<void()> func, int ms,
        std::optional<std::string> timerId = std::nullopt) override;
    void cancel(std::string timerId) override;

    void submit(BaseJob job,
        std::function<void(Response)> callback) override;

    void stop();

private:
    struct Private;
    std::unique_ptr<Private> m_d;
};
