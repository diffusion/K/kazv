/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QJsonObject>

#include <lager/reader.hpp>
#include <lager/extra/qt.hpp>

#include <base/types.hpp>

class MatrixSticker : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<Kazv::json> m_sticker;

public:
    explicit MatrixSticker(lager::reader<std::string> shortCode, lager::reader<Kazv::json> sticker, QObject *parent = 0);
    ~MatrixSticker() override;

    LAGER_QT_READER(QString, shortCode);
    LAGER_QT_READER(QString, body);
    LAGER_QT_READER(QString, mxcUri);
    LAGER_QT_READER(QJsonObject, info);

    Q_INVOKABLE QJsonObject makeEventJson() const;
};
