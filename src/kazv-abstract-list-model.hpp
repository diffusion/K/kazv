/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <QObject>
#include <QQmlEngine>
#include <QAbstractListModel>
#include <lager/extra/qt.hpp>

class KazvAbstractListModel : public QAbstractListModel
{
    Q_OBJECT

    int m_internalCount;

public:
    enum InitMethod {
        InitNow,
        InitLater,
    };

    explicit KazvAbstractListModel(lager::reader<int> count, InitMethod initMethod = InitNow, QObject *parent = 0);
    explicit KazvAbstractListModel(QObject *parent = 0);
    ~KazvAbstractListModel() override;

    LAGER_QT_READER(int, count);

    int rowCount(const QModelIndex &index) const override;
    QVariant data(const QModelIndex &index, int role) const override;

protected:
    void initCountCursor(lager::reader<int> count, InitMethod initMethod = InitNow);

private Q_SLOTS:
    void updateInternalCount();
};
