/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <lager/extra/qt.hpp>
#include <room/room.hpp>
#include "kazv-abstract-list-model.hpp"
Q_MOC_INCLUDE("matrix-event-reaction.hpp")

class MatrixEventReaction;

class MatrixEventReactionListModel : public KazvAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    lager::reader<QMap<QString /* key */, Kazv::EventList>> m_reactions;
    lager::reader<QList<QString>> m_reactionKeys;
public:
    explicit MatrixEventReactionListModel(
        Kazv::Room room,
        lager::reader<std::string> parentEventId,
        QObject *parent = 0);

    ~MatrixEventReactionListModel() override;

    Q_INVOKABLE MatrixEventReaction *at(int index) const;
};
