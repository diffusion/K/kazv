/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <string>
#include <immer/array.hpp>
#include <client.hpp>

inline const auto USER_GIVEN_NICKNAME_EVENT_TYPES = immer::array<std::string>{
    "work.banananet.msc3865.user_given.user.displayname"
};

inline auto userGivenNicknameMapFor(const Kazv::Client &c)
{
    return c.accountData()
        .map([](const auto &accountData) {
            for (const auto &type : USER_GIVEN_NICKNAME_EVENT_TYPES) {
                if (accountData.count(type) > 0) {
                    return accountData[type];
                }
            }
            return Kazv::Event();
        });
}
