/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include "meta-types.hpp"
#include "kazv-platform.hpp"

KazvMetaTypeRegistration::KazvMetaTypeRegistration()
    : m_kazvEvent(qRegisterMetaType<Kazv::KazvEvent>())
    , m_matrixStickerPackSource(qRegisterMetaType<MatrixStickerPackSource>())
{
}
