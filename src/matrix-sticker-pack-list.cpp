/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <lager/lenses/optional.hpp>
#include <lager/lenses/at.hpp>

#include "matrix-sticker-pack.hpp"
#include "matrix-sticker-pack-list.hpp"
#include "matrix-sticker-pack-list-p.hpp"

using namespace Kazv;
using namespace Qt::Literals::StringLiterals;

using ImagePackRoomMap = immer::map<std::string/* room id */, immer::map<std::string/* */, json>>;

lager::reader<immer::flex_vector<MatrixStickerPackSource>> getEventsFromClient(Client client)
{
    lager::reader<Event> userEmotes = client.accountData()[accountDataEventType][lager::lenses::or_default];
    return lager::with(
        userEmotes,
        client.accountData()[imagePackRoomsEventType][lager::lenses::or_default]
        // The following jsonAtOr performs the validation for us:
        // if the content does not conform to the string-to-string-to-anything map, return an empty one
        .xform(eventContent | jsonAtOr("rooms", ImagePackRoomMap{})),
        client.rooms()).map([](
            const Event &userEmotes,
            const ImagePackRoomMap &emoteRooms,
            const immer::map<std::string, RoomModel> &rooms) {
            // This transformation function takes O(|room sticker packs specified in account data|).
            auto res = immer::flex_vector<MatrixStickerPackSource>{
                {MatrixStickerPackSource::AccountData, accountDataEventType, userEmotes, "", ""}
            }.transient();

            for (const auto &[roomId, stateKeyMap] : emoteRooms) {
                for (const auto &[stateKey, _] : stateKeyMap) {
                    res.push_back(MatrixStickerPackSource{
                        MatrixStickerPackSource::RoomState,
                        roomStateEventType,
                        rooms[roomId].stateEvents[KeyOfState{roomStateEventType, stateKey}],
                        roomId,
                        stateKey,
                    });
                }
            }

            return res.persistent();
    }).make();
}

lager::reader<immer::flex_vector<MatrixStickerPackSource>> getEventsFromRoom(Room room)
{
    return lager::with(room.roomId(), room.stateEvents())
        .map([](const auto &roomId, const auto &state) {
            return intoImmer(
                immer::flex_vector<MatrixStickerPackSource>{},
                zug::filter([](const auto &pair) {
                    return pair.first.type == roomStateEventType;
                })
                | zug::map([roomId](const auto &pair) {
                    return MatrixStickerPackSource{
                        MatrixStickerPackSource::RoomState,
                        roomStateEventType,
                        pair.second,
                        roomId,
                        pair.first.stateKey,
                    };
                }),
                state
            );
        });
}

static QString getPackDisplayName(const MatrixStickerPackSource &source)
{
    auto content = source.event.content().get();
    if (content.contains("/pack/display_name"_json_pointer)
        && content["/pack/display_name"_json_pointer].is_string()) {
        return QString::fromStdString(
            content["/pack/display_name"_json_pointer]
            .template get<std::string>());
    }
    return u""_s;
}

static QJsonValue sourceToPacks(const immer::flex_vector<MatrixStickerPackSource> &sources)
{
    QJsonArray res;
    for (const auto &source : sources) {
        QJsonObject obj{
            {u"source"_s, source.source},
            {u"isAccountData"_s, source.source == MatrixStickerPackSource::AccountData},
            {u"isState"_s, source.source == MatrixStickerPackSource::RoomState},
            {u"eventType"_s, QString::fromStdString(source.eventType)},
            {u"roomId"_s, QString::fromStdString(source.roomId)},
            {u"stateKey"_s, QString::fromStdString(source.stateKey)},
            {u"packName"_s, getPackDisplayName(source)},
        };
        res.append(obj);
    }
    return res;
}

static auto defaultSource = MatrixStickerPackSource{
    MatrixStickerPackSource::AccountData,
    accountDataEventType,
    Event(),
    "",
    "",
};

MatrixStickerPackList::MatrixStickerPackList(Client client, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_events(getEventsFromClient(client))
    , LAGER_QT(packs)(m_events.map(sourceToPacks))
{
    initCountCursor(m_events.map([](const auto &events) {
        return static_cast<int>(events.size());
    }));
}

MatrixStickerPackList::MatrixStickerPackList(Room room, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_events(getEventsFromRoom(room))
    , LAGER_QT(packs)(m_events.map(sourceToPacks))
{
    initCountCursor(m_events.map([](const auto &events) {
        return static_cast<int>(events.size());
    }));
}

MatrixStickerPackList::~MatrixStickerPackList() = default;

MatrixStickerPack *MatrixStickerPackList::at(int index) const
{
    return new MatrixStickerPack(m_events.map([index](const auto &events) {
        if (events.size() > std::size_t(index)) {
            return events[index];
        } else {
            return defaultSource;
        }
    }));
}

MatrixStickerPack *MatrixStickerPackList::packFor(const QJsonValue &desc) const
{
    auto source = MatrixStickerPackSource::Source(desc.toObject()[u"source"_s].toInt());
    auto eventType = desc.toObject()[u"eventType"_s].toString().toStdString();
    auto roomId = desc.toObject()[u"roomId"_s].toString().toStdString();
    auto stateKey = desc.toObject()[u"stateKey"_s].toString().toStdString();

    return new MatrixStickerPack(m_events.map([=](const auto &events) {
        auto it = std::find_if(events.begin(), events.end(), [=](const auto &s) {
            return s.source == source
                && s.eventType == eventType
                && s.roomId == roomId
                && s.stateKey == stateKey;
        });
        if (it == events.end()) {
            return defaultSource;
        } else {
            return *it;
        }
    }));
}
