/*
 * This file is part of kazv-js-deps.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import { readFileSync } from 'node:fs';
import { argv, exit } from 'node:process';
import { parseResources, validate, diffCore } from './lint-l10n-lib.mjs';

const l10nDir = argv[2];
const config = JSON.parse(
  readFileSync('./l10n-config.json', { encoding: 'UTF-8' })
);
const { coreLanguages } = config;

const ftlParsed = parseResources(l10nDir);

let success = true;

const parseErrors = validate(ftlParsed);

if (parseErrors.length) {
  success = false;
  console.error('ftl files contain errors:');
  parseErrors.forEach(e => console.error(`- ${e}`));
}

const diffErrors = diffCore(ftlParsed, coreLanguages);
if (diffErrors.length) {
  success = false;
  console.error('Messages in core languages differ:');
  diffErrors.forEach(e => console.error(`- ${e}`));
}

exit(success ? 0 : 1);
