#!/bin/bash

npx babel node_modules/@fluent/bundle/index.js > transformed-libs/fluent-bundle.js
npx babel node_modules/@fluent/sequence/index.js > transformed-libs/fluent-sequence.js
npx babel node_modules/@fluent/langneg/index.js > transformed-libs/fluent-langneg.js
npx babel Intl.js/dist/Intl.complete.js > transformed-libs/bundled-deps.js
