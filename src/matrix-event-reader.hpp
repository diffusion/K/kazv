/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include "matrix-room-member.hpp"

class MatrixEventReader : public MatrixRoomMember
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

public:
    explicit MatrixEventReader(lager::reader<Kazv::Event> memberEvent, lager::reader<Kazv::Timestamp> timestamp, QObject *parent = 0);
    ~MatrixEventReader() override;

    LAGER_QT_READER(qint64, timestamp);
    LAGER_QT_READER(QString, formattedTime);
};
