/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>
#include <QSignalSpy>

#include <matrix-sdk.hpp>
#include <matrix-device-list.hpp>
#include <matrix-device.hpp>
#include <matrix-promise.hpp>

#include "test-model.hpp"
#include "test-utils.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;

class MatrixSdkTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testDevicesOfUser();
    void testSetDeviceTrustLevel();
    void testValidateHomeserverUrl();
};

void MatrixSdkTest::testDevicesOfUser()
{
    auto model = makeTestModel();
    std::unique_ptr<MatrixSdk> sdk{new MatrixSdk(model, /* testing = */ true)};

    auto devices = toUniquePtr(sdk->devicesOfUser(QStringLiteral("@test1:test1.org")));
    QVERIFY(devices->count() == 2);
    auto dev = toUniquePtr(devices->at(0));
    QVERIFY(dev->deviceId() == u"device1"_s);
    QVERIFY(dev->trustLevel() == u"unseen"_s);
}

void MatrixSdkTest::testSetDeviceTrustLevel()
{
    auto model = makeTestModel();
    std::unique_ptr<MatrixSdk> sdk{new MatrixSdk(model, /* testing = */ true)};

    sdk->startThread();

    auto promise = toUniquePtr(
        sdk->setDeviceTrustLevel(
            QStringLiteral("@test1:test1.org"),
            QStringLiteral("device1"),
            QStringLiteral("verified")
        )
    );

    auto spy = QSignalSpy(promise.get(), &MatrixPromise::succeeded);
    spy.wait();

    auto devices = toUniquePtr(sdk->devicesOfUser(QStringLiteral("@test1:test1.org")));
    QVERIFY(devices->count() == 2);
    auto dev = toUniquePtr(devices->at(0));
    QVERIFY(dev->deviceId() == u"device1"_s);
    QTRY_VERIFY(dev->trustLevel() == u"verified"_s); // wait for the change to propagate to this thread
}

void MatrixSdkTest::testValidateHomeserverUrl()
{
    auto v = [](const QString &u) {
        return QString::fromStdString(MatrixSdk::validateHomeserverUrl(u));
    };

    QCOMPARE(v(u"example.com"_s), u"https://example.com"_s);
    QCOMPARE(v(u"to"_s), u"https://to"_s);
    QVERIFY(v(u""_s).isEmpty());
    QVERIFY(v(u"/usr/libexec"_s).isEmpty());
    QCOMPARE(v(u"ea.pl"_s), u"https://ea.pl"_s);
}

QTEST_MAIN(MatrixSdkTest)

#include "matrix-sdk-test.moc"
