/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>

#include <matrix-room-timeline.hpp>
#include <matrix-sdk.hpp>
#include <matrix-room-list.hpp>
#include <matrix-room.hpp>
#include <matrix-event.hpp>
#include <matrix-utils.hpp>
#include "test-model.hpp"
#include "test-utils.hpp"
#include "factory.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
using namespace Kazv::Factory;

class MatrixRoomListTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testRoomList();
    void testSorted();
    void testSortedWithTag();
    void testFilter();
    void testPriority();
};

static auto tagEvent = Event{R"({
  "content": {
    "tags": {"m.favourite": {}}
  },
  "type": "m.tag"
})"_json};

void MatrixRoomListTest::testRoomList()
{
    auto model = makeTestModel();
    RoomModel room;
    room.roomId = "!test:tusooa.xyz";
    room.accountData = room.accountData.set("m.tag", tagEvent);
    model.client.roomList.rooms = model.client.roomList.rooms.set(room.roomId, room);

    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());

    QCOMPARE(roomList->count(), 2);

    roomList->setTagId(u"m.favourite"_s);
    QCOMPARE(roomList->count(), 1);

    roomList->setTagId(u"u.xxx"_s);
    QCOMPARE(roomList->count(), 0);
}

void MatrixRoomListTest::testSorted()
{
    auto model = makeTestModel();
    model.client = makeClient();
    auto room1 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1000)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 2000))
        })
        | withRoomMembership(Join)
    );
    auto room2 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 700)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1500))
        })
        | withRoomMembership(Join)
    );
    auto room3 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1500)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 2500))
        })
        | withRoomMembership(Join)
    );
    auto room4 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 300)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 500))
        })
        | withRoomMembership(Invite)
    );

    withRoom(room1)(model.client);
    withRoom(room2)(model.client);
    withRoom(room3)(model.client);
    withRoom(room4)(model.client);

    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());

    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room4.roomId));
    QCOMPARE(roomList->roomIdAt(1), QString::fromStdString(room3.roomId));
    QCOMPARE(roomList->roomIdAt(2), QString::fromStdString(room1.roomId));
    QCOMPARE(roomList->roomIdAt(3), QString::fromStdString(room2.roomId));
}

void MatrixRoomListTest::testSortedWithTag()
{
    auto model = makeTestModel();
    model.client = makeClient();
    auto room1 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1000)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 2000))
        })
        | withRoomAccountData({tagEvent})
    );
    auto room2 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 700)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1500))
        })
        | withRoomAccountData({tagEvent})
    );
    auto room3 = makeRoom(
        withRoomTimeline({
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 1500)),
            makeEvent(withEventKV("/origin_server_ts"_json_pointer, 2500))
        })
        | withRoomAccountData({tagEvent})
    );

    withRoom(room1)(model.client);
    withRoom(room2)(model.client);
    withRoom(room3)(model.client);

    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());

    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room3.roomId));
    QCOMPARE(roomList->roomIdAt(1), QString::fromStdString(room1.roomId));
    QCOMPARE(roomList->roomIdAt(2), QString::fromStdString(room2.roomId));
}

void MatrixRoomListTest::testFilter()
{
    auto model = makeTestModel();
    model.client = makeClient();
    auto room1 = makeRoom(
        withRoomState({makeEvent(withStateKey("") | withEventType("m.room.name") | withEventContent(json{{"name", "some room"}}))})
    );
    auto room2 = makeRoom(
        withRoomState({makeEvent(withStateKey("") | withEventType("m.room.name") | withEventContent(json{{"name", "some other room"}}))})
    );
    auto room3 = makeRoom(
        withRoomId("!some:example.org")
        | withRoomState({
            makeMemberEvent(withStateKey("@foo:tusooa.xyz") | withEventKV(json::json_pointer("/content/displayname"), "User aaa")),
            makeMemberEvent(withStateKey("@bar:tusooa.xyz") | withEventKV(json::json_pointer("/content/displayname"), "User bbb")),
        })
    );
    room3.heroIds = immer::flex_vector<std::string>{"@foo:tusooa.xyz", "@bar:tusooa.xyz"};

    withRoom(room1)(model.client);
    withRoom(room2)(model.client);
    withRoom(room3)(model.client);
    auto nicknameEvent = makeEvent(
        withEventType(USER_GIVEN_NICKNAME_EVENT_TYPES[0])
        | withEventContent(json{{"@foo:tusooa.xyz", "xxxx"}})
    );
    withAccountData({ nicknameEvent })(model.client);

    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());
    roomList->setfilter(u"some"_s);

    QCOMPARE(roomList->count(), 2);
    QCOMPARE(roomList->roomIds().contains(QString::fromStdString(room2.roomId)), true);
    QCOMPARE(roomList->roomIds().contains(QString::fromStdString(room1.roomId)), true);

    roomList->setfilter(u"!some:example.org"_s);

    QCOMPARE(roomList->count(), 1);
    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room3.roomId));

    roomList->setfilter(u"foo"_s);

    QCOMPARE(roomList->count(), 1);
    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room3.roomId));

    roomList->setfilter(u"aaa"_s);

    QCOMPARE(roomList->count(), 1);
    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room3.roomId));

    // By custom nickname of user (@foo:tusooa.xyz)
    roomList->setfilter(u"xxxx"_s);

    QCOMPARE(roomList->count(), 1);
    QCOMPARE(roomList->roomIdAt(0), QString::fromStdString(room3.roomId));
}

void MatrixRoomListTest::testPriority()
{
    auto model = makeTestModel();
    RoomModel roomFavourited = makeRoom(
        withRoomId("!favourite:tusooa.xyz")
        | withRoomAccountData(immer::flex_vector{
            makeEvent(withEventContent(tagEvent))}));
    RoomModel roomInvited = makeRoom(
        withRoomId("!invite:tusooa.xyz")
        | withRoomMembership(RoomMembership::Invite));
    model.client.roomList.rooms = model.client.roomList.rooms.set(
        roomInvited.roomId, roomInvited);
    model.client.roomList.rooms = model.client.roomList.rooms.set(
        roomFavourited.roomId, roomFavourited);

    std::unique_ptr<MatrixSdk> sdk{makeTestSdk(model)};
    auto roomList = toUniquePtr(sdk->roomList());

    QCOMPARE(roomList->count(), 3);
    // Priority: Invite > Favourite
    QCOMPARE(roomList->roomIdAt(0), u"!invite:tusooa.xyz"_s);
    QCOMPARE(roomList->roomIdAt(1), u"!favourite:tusooa.xyz"_s);
}

QTEST_MAIN(MatrixRoomListTest)

#include "matrix-room-list-test.moc"
