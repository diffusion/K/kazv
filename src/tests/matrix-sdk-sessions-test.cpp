/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <kazv-platform.hpp>
#include <memory>
#include <filesystem>
#include <fstream>
#include <QObject>
#include <QtTest>
#include <QProcess>
#include <QCoreApplication>
#include <crypto/base64.hpp>
#include "test-temp.hpp"
#include "matrix-sdk-sessions-test.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
namespace Fs = std::filesystem;
using StdPath = Fs::path;

void MatrixSdkSessionsTest::initTestCase()
{
    auto dir = StdPath(kazvTestTempDir()) / "sessions-test";
    m_userDataDir = dir.string();
}

void MatrixSdkSessionsTest::clearDir()
{
    QVERIFY(!m_userDataDir.empty());
    std::error_code ec;
    Fs::remove_all(StdPath(m_userDataDir), ec);
    QVERIFY(!ec);
}

void MatrixSdkSessionsTest::init() { clearDir(); }

void MatrixSdkSessionsTest::cleanup() { clearDir(); }

void MatrixSdkSessionsTest::createSession(std::string userId, std::string deviceId)
{
    auto sessionsDir = StdPath(m_userDataDir) / "sessions";
    SdkModel model;
    model.client.userId = userId;
    model.client.deviceId = deviceId;
    {
        auto sdk = makeMatrixSdk(model, /* testing = */ false);
        sdk->serializeToFile();
    }
    // Not ideal, but this is the only way to wait for
    // MatrixSdkPravite to be destroyed.
    QTest::qWait(100);
}

void MatrixSdkSessionsTest::testListSessions()
{
    createSession("@mew:example.com", "device1");
    createSession("@mew:example.com", "device2");
    createSession("@mew2:example.com", "device3");

    auto sdk = makeMatrixSdk();
    QSet<QString> expected{
        QStringLiteral("@mew:example.com/device1"),
        QStringLiteral("@mew:example.com/device2"),
        QStringLiteral("@mew2:example.com/device3"),
    };

    auto sessionList = sdk->allSessions();
    auto sessionSet = QSet<QString>(sessionList.begin(), sessionList.end());
    QCOMPARE(sessionSet, expected);
}

void MatrixSdkSessionsTest::testListLegacySessions()
{
// This test cannot be run on Windows, because the file name
// there cannot contain ':'.
#if KAZV_IS_WINDOWS
    QSKIP("Skipping because there are no legacy sessions on Windows");
#endif

    createSession("@mew:example.com", "device1");
    auto legacySession = StdPath(m_userDataDir) / "sessions" / "@mew3:example.com" / "device4";
    {
        std::ofstream s{legacySession / "store"};
        s << "";
    }
    auto sdk = makeMatrixSdk();

    QStringList expected{
        QStringLiteral("@mew:example.com/device1"),
    };

    QCOMPARE(sdk->allSessions(), expected);
}

void MatrixSdkSessionsTest::testLoadSession()
{
    createSession("@mew:example.com", "device4");

    auto sdk = makeMatrixSdk();
    auto res = sdk->loadSession(u"@mew:example.com/device4"_s);
    QCOMPARE(res, MatrixSdk::SessionLoadSuccess);
    QCOMPARE(sdk->userId(), QStringLiteral("@mew:example.com"));
    QCOMPARE(sdk->deviceId(), QStringLiteral("device4"));
}

void MatrixSdkSessionsTest::testSessionLock()
{
#if KAZV_IS_WINDOWS
    QSKIP("Skipping because session lock is not yet supported on Windows");
#else
    createSession("@mew:example.com", "device5");

    auto program = QCoreApplication::applicationDirPath() + QStringLiteral("/matrix-sdk-session-loader");
    QProcess proc1;
    QStringList args{
        QString::fromStdString(m_userDataDir),
        QStringLiteral("@mew:example.com/device5"),
    };
    proc1.start(program, args);
    proc1.waitForReadyRead();
    auto line = proc1.readLine();
    QCOMPARE(line, QByteArray("loaded session\n"));

    QProcess proc2;
    proc2.start(program, args);
    auto res = proc2.waitForFinished();
    QVERIFY(res);
    QCOMPARE(proc2.exitStatus(), QProcess::NormalExit);
    QCOMPARE(proc2.exitCode(), 1);

    proc1.kill();
#endif
}

void MatrixSdkSessionsTest::testSaveSessionFailure()
{
#if KAZV_IS_WINDOWS
    QSKIP("Skipping because session the condition cannot be easily satisfied on Windows");
#endif

    auto userId = std::string("@mew:example.com");
    auto deviceId = std::string("device1");
    createSession(userId, deviceId);

    auto sessionDir = sessionDirForUserAndDeviceId(Fs::path(m_userDataDir), userId, deviceId);
    // there is store, metadata and lock under the sessionDir

    std::error_code err;
    // make the sessionDir readonly, so that new file cannot be created
    Fs::permissions(
        sessionDir,
        Fs::perms::owner_read | Fs::perms::owner_exec,
        Fs::perm_options::replace,
        err);
    QVERIFY(!err);
    err.clear();

    // a model that is slightly different from the original one
    SdkModel model;
    model.client.userId = userId;
    model.client.deviceId = deviceId;
    model.client.serverUrl = "https://example.com";

    {
        auto sdk = makeMatrixSdk(model, /* testing = */ false);
        // this should fail
        sdk->serializeToFile();
    }
    // Not ideal, but this is the only way to wait for
    // MatrixSdkPravite to be destroyed.
    QTest::qWait(100);

    auto sdk = makeMatrixSdk();
    auto res = sdk->loadSession(QString::fromStdString(userId + "/" + deviceId));
    QCOMPARE(res, MatrixSdk::SessionLoadSuccess);
    // verify that the original data is kept
    QCOMPARE(sdk->serverUrl(), u""_s);

    // before cleaning up, add write permissions back so the files can be removed
    Fs::permissions(
        sessionDir,
        Fs::perms::owner_read | Fs::perms::owner_write | Fs::perms::owner_exec,
        Fs::perm_options::replace,
        err);
    QVERIFY(!err);
    err.clear();
}

#ifndef MATRIX_SDK_SESSIONS_TEST_NO_MAIN
QTEST_MAIN(MatrixSdkSessionsTest)
#endif
