/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <memory>

#include <QtTest>

#include <testfixtures/factory.hpp>

#include "kazv-markdown.hpp"
#include "matrix-room.hpp"

using namespace Qt::Literals::StringLiterals;
using namespace Kazv;
using namespace Kazv::Factory;

class KazvMarkdownTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testMarkdown();
    void testMentionInText();
    void testMentionInBlock();
    void testNoMentionInCode();
    void testNoMentionInLink();
    void testMentionList();
    void testMakeMentionsEvent();
};

void KazvMarkdownTest::testMarkdown()
{
    QCOMPARE(
        QString::fromStdString(markdownToHtml("mew mew").html),
        QString::fromStdString("<p>mew mew</p>")
    );
}

void KazvMarkdownTest::testMentionInText()
{
    QCOMPARE(
        QString::fromStdString(markdownToHtml("@foo:example.com").html),
        QString::fromStdString("<p><a href=\"https://matrix.to/#/@foo:example.com\">@foo:example.com</a></p>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("mew mew @foo:example.com").html),
        QString::fromStdString("<p>mew mew <a href=\"https://matrix.to/#/@foo:example.com\">@foo:example.com</a></p>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("mew mew@foo:example.com").html),
        QString::fromStdString("<p>mew mew@foo:example.com</p>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("mew mew -@foo:example.com").html),
        QString::fromStdString("<p>mew mew -@foo:example.com</p>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("mew mew @foo:example.com_").html),
        QString::fromStdString("<p>mew mew @foo:example.com_</p>")
    );
}

void KazvMarkdownTest::testMentionInBlock()
{
    QCOMPARE(
        QString::fromStdString(markdownToHtml("<div>\nmew mew @foo:example.com\n</div>").html),
        QString::fromStdString("<div>\nmew mew <a href=\"https://matrix.to/#/@foo:example.com\">@foo:example.com</a>\n</div>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("<div>\nmew mew@foo:example.com\n</div>").html),
        QString::fromStdString("<div>\nmew mew@foo:example.com\n</div>")
    );
}

void KazvMarkdownTest::testNoMentionInCode()
{
    QCOMPARE(
        QString::fromStdString(markdownToHtml("```\nmew@foo:example.com mew @foo:example.com\n```").html),
        QString::fromStdString("<pre><code>mew@foo:example.com mew @foo:example.com\n</code></pre>")
    );

    QCOMPARE(
        QString::fromStdString(markdownToHtml("`mew@foo:example.com mew @foo:example.com`").html),
        QString::fromStdString("<p><code>mew@foo:example.com mew @foo:example.com</code></p>")
    );
}

void KazvMarkdownTest::testNoMentionInLink()
{
    QCOMPARE(
        QString::fromStdString(markdownToHtml("[@foo:example.com](https://example.com/foo)").html),
        QString::fromStdString("<p><a href=\"https://example.com/foo\">@foo:example.com</a></p>")
    );
}

void KazvMarkdownTest::testMentionList()
{
    auto result = markdownToHtml("@foo:example.com @bar:example.com @foo:example.com");
    QCOMPARE(result.mentions.size(), 2);
    QVERIFY(std::find(result.mentions.begin(), result.mentions.end(), "@foo:example.com") != result.mentions.end());
    QVERIFY(std::find(result.mentions.begin(), result.mentions.end(), "@bar:example.com") != result.mentions.end());
}

void KazvMarkdownTest::testMakeMentionsEvent()
{
    auto replyToEvent = makeEvent(withEventKV(json::json_pointer("/sender"), "@foo:example.com"));
    auto e1 = makeTextMessageJson(u"@foo:example.com mew"_s, u""_s, u""_s, Kazv::Event());

    QVERIFY(e1["content"]["m.mentions"]["user_ids"] == json{"@foo:example.com"});

    auto e2 = makeTextMessageJson(u"@foo:example.com mew"_s, u""_s, QString::fromStdString(replyToEvent.id()), replyToEvent);
    QVERIFY(e2["content"]["m.mentions"]["user_ids"] == json{"@foo:example.com"});

    auto e3 = makeTextMessageJson(u"@bar:example.com mew"_s, u""_s, QString::fromStdString(replyToEvent.id()), replyToEvent);
    QVERIFY((e3["content"]["m.mentions"]["user_ids"] == json{"@bar:example.com", "@foo:example.com"}));
}

QTEST_MAIN(KazvMarkdownTest)

#include "kazv-markdown-test.moc"
