/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>
#include <memory>
#include <QObject>
#include <matrix-sdk.hpp>

class MatrixSdkSessionsTest : public QObject
{
    Q_OBJECT

private:
    std::string m_userDataDir;

    void clearDir();
    void createSession(std::string userId, std::string deviceId);

public:
    template<class ...Args>
    static std::unique_ptr<MatrixSdk> makeMatrixSdkImpl(std::string userDataDir, Args &&...args)
    {
        std::unique_ptr<MatrixSdk> sdk(new MatrixSdk(std::forward<Args>(args)...));
        sdk->setUserDataDir(userDataDir);
        return sdk;
    }

    template<class ...Args>
    std::unique_ptr<MatrixSdk> makeMatrixSdk(Args &&...args)
    {
        return makeMatrixSdkImpl(m_userDataDir, std::forward<Args>(args)...);
    }

private Q_SLOTS:
    void initTestCase();
    void init();
    void cleanup();

    void testListSessions();
    void testListLegacySessions();
    void testLoadSession();
    void testSessionLock();
    void testSaveSessionFailure();
};
