/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <iostream>
#include <QString>
#include "matrix-sdk-sessions-test.hpp"

int main([[maybe_unused]] int argc, char *argv[])
{
    std::string userDataDir = argv[1];
    std::string sessionName = argv[2];
    auto sdk = MatrixSdkSessionsTest::makeMatrixSdkImpl(userDataDir);
    auto res = sdk->loadSession(QString::fromStdString(sessionName));
    if (res != MatrixSdk::SessionLoadSuccess) {
        std::cout << "cannot load session" << std::endl;
        return res == MatrixSdk::SessionLockFailed ? 1 : 2;
    }

    std::cout << "loaded session" << std::endl;
    while (1) {}
    return 0;
}
