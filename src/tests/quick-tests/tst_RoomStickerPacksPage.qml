/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtTest
import moe.kazv.mxc.kazv as MK
import org.kde.kirigami as Kirigami
import '../../contents/ui/room-settings' as KazvRS
import '../../js/matrix-helpers.js' as MatrixHelpers
import 'test-helpers.js' as JsHelpers
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  KazvRS.RoomStickerPacksPage {
    id: page
    anchors.fill: parent
    room: QtObject {
      function state(type, stateKey) {
        return {
          type,
          stateKey,
          content: stateKey === 'foo' ? {
            images: {
              'mew': {},
            },
          } : stateKey === 'bar' ? {
            pack: {
              'some': 'others',
            },
          } : {},
        };
      }

      property var sendStateEvent: mockHelper.promise()
    }
  }

  TestCase {
    id: roomStickerPacksPageTest
    name: 'RoomStickerPacksPageTest'
    when: windowShown

    function init() {
      item.mockHelper.clearAll();
    }

    function test_addPack_data() {
      return [
        {
          succ: true,
          stateKey: '',
          packContent: {
            pack: {
              display_name: 'some name',
            },
          },
        },
        {
          succ: false,
          stateKey: '',
          packContent: {
            pack: {
              display_name: 'some name',
            },
          },
        },
        {
          succ: true,
          stateKey: 'foo',
          packContent: {
            images: {
              'mew': {},
            },
            pack: {
              display_name: 'some name',
            },
          },
        },
        {
          succ: true,
          stateKey: 'bar',
          packContent: {
            pack: {
              'some': 'others',
              display_name: 'some name',
            },
          },
        },
      ];
    }

    function test_addPack(data) {
      findChild(page, 'addStickerPackAction').trigger();
      tryVerify(() => findChild(page, 'stickerPackName').visible);
      findChild(page, 'stickerPackName').text = 'some name';
      findChild(page, 'stickerPackStateKey').text = data.stateKey;
      page.addStickerPackPopup.accept();
      tryVerify(() => page.room.sendStateEvent.calledTimes() === 1);
      verify(JsHelpers.deepEqual(
        page.room.sendStateEvent.lastArgs(),
        [{
          type: MatrixHelpers.roomImagePackEventType,
          state_key: data.stateKey,
          content: data.packContent,
        }],
      ));

      page.room.sendStateEvent.lastRetVal().resolve(data.succ, {});
      compare(item.showPassiveNotification.calledTimes(), 1);
    }
  }
}
