/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 April Simone <apr3vau@outlook.com>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtTest

import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  Kazv.ReactToEventPopup {
    id: popup
  }
  TestCase {
    id: reactToEventPopupTest
    name: 'ReactToEventPopupTest'
    when: windowShown
    function test_react() {
      skip("Skipped as it requires further change.");
      popup.open();
      const grid = findChild(popup, 'emojiGrid');
      waitForRendering(grid);
      mouseClick(grid.currentItem);
      tryVerify(() => findChild(popup, 'reactionTextInput').text === grid.currentItem.str);
    }
  }
}
