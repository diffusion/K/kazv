/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import '.' as TestHelpers
import '../test-helpers.js' as Helpers

/**
 * Helper class for conveniently mocking functions.
 *
 * The mocks record the arguments and return values of the functions.
 *
 * - `MockHelper.func(f, t)` returns a mock of the function `f`, with
 *   its arguments recorded transformed by `t`.
 *   The transform can be either:
 *   - A function, which means the recorded arguments will be
 *     `t(originalArgs)`.
 *   - An array, which means the recorded arguments will be an
 *     object whose keys are the items of the array and the values are
 *     the recorded arguments.
 *     - For example, if the provided transform is `['foo', 'bar']` and
 *       the mock is called with `(1, 2, 3)`, the recorded arguments
 *       will be `{ foo: 1, bar: 2 }`. (Note that the additional argument
 *       3 is ignored)
 *   - undefined, which means the arguments will be recorded as-is
 *     (equivalent to providing the identity function).
 * - `MockHelper.promise(t)` returns a mock of a function returning
 *   a MatrixPromiseMock, with its arguments recorded transformed by `t`.
 * - `MockHelper.noop(t)` returns a mock of a function doing nothing,
 *   with its arguments recorded transformed by `t`.
 * - `MockHelper.clearAll()` resets the recorded information of all
 *   mocks *created by this MockHelper*.
 *
 * The mocks have the following interface:
 * - `mock()` calls the function and returns the data.
 * - `mock.clear()` resets the recorded information (times called,
 *   arguments, return values).
 * - `mock.calledTimes()` returns the number of times the mock gets called.
 * - `mock.lastArgs()` returns the arguments of the last call, as an array.
 * - `mock.args` returns a list of arguments of each call, ordered from the
 *   earliest call to the last call.
 * - `mock.lastRetVal()` returns the return value of the last call.
 * - `mock.retVals` returns a list of return values of each call, ordered
 *   from the earliest call to the last call.
 */
QtObject {
  id: mockHelper

  property var promiseComp: Component {
    TestHelpers.MatrixPromiseMock {}
  }
  property var mocks: []

  function clearAll() {
    mocks.forEach((mock) => {
      mock.clear();
    });
  }

  function func(f, t) {
    const ret = Helpers.mockFunction(f, t);
    mocks.push(ret);
    return ret;
  }

  function createPromise() {
    return promiseComp.createObject(mockHelper);
  }

  function promise(t) {
    return func(createPromise, t);
  }

  function noop(t) {
    return func(() => {}, t);
  }
}
