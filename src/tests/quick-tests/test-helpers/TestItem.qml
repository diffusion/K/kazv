/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtTest
import '.' as QmlHelpers
import '../test-helpers.js' as JsHelpers

Item {
  id: item
  width: 800
  height: 600

  property var l10n: JsHelpers.fluentMock
  property var mockHelper: QmlHelpers.MockHelper {}
  property var promiseComp: Component {
    QmlHelpers.MatrixPromiseMock {}
  }
  property var matrixSdk: QmlHelpers.MatrixSdkMock {
  }
  property var sdkVars: QtObject {
    property var roomList: ({})
    property var userGivenNicknameMap: QtObject {
      property var map: ({
        '@foo:tusooa.xyz': 'something',
      })
      property var setAndUpload: mockHelper.promise()
    }
  }
  property var kazvIOManager: QmlHelpers.KazvIOManagerMock {}
  property var showPassiveNotification: mockHelper.noop()
  property var pageStack: QtObject {
    property var removePage: mockHelper.noop()
  }

  function applicationWindow() {
    return Window.window;
  }

  Component.onCompleted: {
    const testFunctions = Object.keys(item).filter(name => name.startsWith('test_'));
    if (testFunctions.length) {
      console.error('There are functions starting with test_ in the root item. It is likely that they belong to the TestCase instead. The functions are:', testFunctions);
      throw new Error('Unexpected test functions');
    }
  }
}
