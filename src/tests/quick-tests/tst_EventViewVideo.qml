/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtQuick.Layouts 1.15
import QtTest 1.0

import moe.kazv.mxc.kazv 0.0 as MK
import org.kde.kirigami 2.13 as Kirigami
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item

  property var videoEvent: ({
    eventId: '',
    sender: '@foo:tusooa.xyz',
    type: 'm.room.message',
    stateKey: '',
    content: {
      msgtype: 'm.image',
      body: 'some body',
      info: {
        h: Kirigami.Units.gridUnit * 10,
        w: Kirigami.Units.gridUnit * 10,
      }
    },
    formattedTime: '4:06 P.M.',
  })

  ColumnLayout {
    anchors.fill: parent
    Kazv.EventView {
      id: eventViewVideo
      event: item.videoEvent
      sender: item.sender
    }

    Kazv.EventView {
      id: eventViewVideoCompact
      compactMode: true
      event: item.videoEvent
      sender: item.sender
    }
  }

  TestCase {
    id: eventViewVideoTest
    name: 'EventViewVideoTest'
    when: windowShown

    function init() {
    }

    function test_videoEvent() {
      verify(!findChild(eventViewVideo, 'summaryLabel').visible);
      verify(findChild(eventViewVideo, 'bodyLabel').visible);
      verify(findChild(eventViewVideoCompact, 'summaryLabel').visible);
      verify(!findChild(eventViewVideoCompact, 'bodyLabel').visible);
      const image = findChild(eventViewVideoCompact, 'mainImage');
      verify(image.height <= Kirigami.Units.gridUnit * 5);
    }
  }
}
