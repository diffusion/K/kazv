/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.3
import QtTest 1.0

import '../../contents/ui/device-mgmt' as KazvDM
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  property var deviceItem: ({
    deviceId: 'some-id',
    displayName: 'name',
    trustLevel: 'unseen'
  })

  KazvDM.Device {
    id: device
  }

  TestCase {
    id: deviceTest
    name: 'DeviceTest'
    when: windowShown

    function initTestCase() {
      device.item = deviceItem;
    }

    function test_device() {
      verify(findChild(device, 'deviceIdLabel').text === 'some-id');
      verify(findChild(device, 'trustLevelLabel').text === l10n.get('device-trust-level-unseen'));
    }
  }
}
