/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtTest 1.0
import org.kde.kirigami 2.20 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK
import '../../contents/ui' as Kazv
import 'test-helpers' as QmlHelpers

QmlHelpers.TestItem {
  id: item
  width: Kirigami.Units.gridUnit * 80
  height: 600

  SignalSpy {
    id: uploadRequestedSpy
    signalName: 'uploadRequested'
  }

  Kazv.ConfirmUploadPopup {
    id: confirmUploadPopup
  }

  function isSheetOpen(sheet) {
    return sheet.opened;
  }

  TestCase {
    id: confirmUploadPopupTest
    name: 'ConfirmUploadPopupTest'
    when: windowShown

    function init() {
      uploadRequestedSpy.clear();
      uploadRequestedSpy.target = confirmUploadPopup;
    }

    function test_singleUpload() {
      confirmUploadPopup.call(['file:///tmp/image.png']);
      tryVerify(() => isSheetOpen(confirmUploadPopup));
      if (MK.KazvUtil.kfQtMajorVersion === 5) {
        confirmUploadPopup.contentItem.parent = item;
      }
      tryVerify(() => findChild(confirmUploadPopup, 'imagePreview').visible);
      mouseClick(findChild(confirmUploadPopup, 'acceptButton'));
      tryVerify(() => !isSheetOpen(confirmUploadPopup));
      verify(uploadRequestedSpy.count === 1);
      verify(uploadRequestedSpy.signalArguments[0][0] === 'file:///tmp/image.png');
    }

    function test_singleUploadCancelled() {
      confirmUploadPopup.call(['file:///tmp/image.png']);
      tryVerify(() => isSheetOpen(confirmUploadPopup));
      if (MK.KazvUtil.kfQtMajorVersion === 5) {
        confirmUploadPopup.contentItem.parent = item;
      }
      tryVerify(() => findChild(confirmUploadPopup, 'imagePreview').visible);
      mouseClick(findChild(confirmUploadPopup, 'cancelButton'));
      tryVerify(() => !isSheetOpen(confirmUploadPopup));
      verify(uploadRequestedSpy.count === 0);
    }

    function test_multiUpload() {
      confirmUploadPopup.call(['file:///tmp/image.png', 'file:///tmp/a.txt']);
      tryVerify(() => isSheetOpen(confirmUploadPopup));
      if (MK.KazvUtil.kfQtMajorVersion === 5) {
        confirmUploadPopup.contentItem.parent = item;
      }
      tryVerify(() => findChild(confirmUploadPopup, 'imagePreview').visible);
      mouseClick(findChild(confirmUploadPopup, 'acceptButton'));
      verify(isSheetOpen(confirmUploadPopup));

      tryVerify(() => !findChild(confirmUploadPopup, 'imagePreview').visible);
      verify(uploadRequestedSpy.count === 1);
      verify(uploadRequestedSpy.signalArguments[0][0] === 'file:///tmp/image.png');
      verify(confirmUploadPopup.currentIndex === 1);
      mouseClick(findChild(confirmUploadPopup, 'acceptButton'));

      tryVerify(() => !isSheetOpen(confirmUploadPopup));
      verify(uploadRequestedSpy.count === 2);
      verify(uploadRequestedSpy.signalArguments[1][0] === 'file:///tmp/a.txt');
    }

    function test_multiUploadCancelled() {
      confirmUploadPopup.call(['file:///tmp/image.png', 'file:///tmp/a.txt']);
      tryVerify(() => isSheetOpen(confirmUploadPopup));
      tryVerify(() => findChild(confirmUploadPopup, 'imagePreview').visible);
      const button = findChild(confirmUploadPopup, 'cancelButton');
      mouseClick(button);
      tryVerify(() => !isSheetOpen(confirmUploadPopup));
      verify(uploadRequestedSpy.count === 0);
    }
  }
}
