/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include <QtTest>
#include <QSignalSpy>
#include <lager/state.hpp>
#include <testfixtures/factory.hpp>
#include "matrix-event-list.hpp"
#include "matrix-event.hpp"
#include "test-utils.hpp"

using namespace Kazv;
using namespace Kazv::Factory;

class MatrixEventListTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testEventList();
};

void MatrixEventListTest::testEventList()
{
    auto eventList = EventList{
        makeEvent(),
        makeEvent(),
    };

    auto state = lager::make_state(eventList, lager::automatic_tag{});

    MatrixEventList list(state);

    QCOMPARE(list.count(), 2);
    auto e1 = toUniquePtr(list.at(0));
    QCOMPARE(e1->eventId().toStdString(), eventList[0].id());
    auto e2 = toUniquePtr(list.at(1));
    QCOMPARE(e2->eventId().toStdString(), eventList[1].id());
}

QTEST_MAIN(MatrixEventListTest)

#include "matrix-event-list-test.moc"
