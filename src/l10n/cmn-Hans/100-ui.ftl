### This file is part of kazv.
### SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
### SPDX-License-Identifier: AGPL-3.0-or-later

app-title = { -kt-app-name }

about-page-title = 关于 { -kt-app-name }
about-copyright = (c) 2020- the Kazv Project
about-display-name = { -kt-app-name } { $version }
about-short-description = 各平台同一的 Matrix 客户端和即时通讯软件
about-license = 以 AGPL 3 或以后版本授权
about-used-libraries = 使用了的库
about-authors = 作者
about-author-email-action = 写电邮
about-author-website-action = 访问网站
about-author-task-maintainer = 维护者
about-author-task-developer = 开发者

user-name-with-id = { $name } （{ $userId }）
user-name-overrided = { $overridedName } （{ $globalName }）

global-drawer-title = { -kt-app-name }
global-drawer-action-switch-account = 切换账号
global-drawer-action-hard-logout = 登出
global-drawer-action-save-session = 保存当前会话
global-drawer-action-configure-shortcuts = 配置快捷键
global-drawer-action-settings = 设置
global-drawer-action-create-room = 创建房间
global-drawer-action-join-room = 加入房间
global-drawer-action-about = 关于 { -kt-app-name }

action-settings-page-title = 配置快捷键
action-settings-shortcut-prompt = 快捷键：{ $shortcut }
action-settings-shortcut-none = （无）
action-settings-shortcut-edit-action = 编辑
action-settings-shortcut-remove-action = 清除
action-settings-shortcut-conflict-modal-title = 冲突的快捷键
action-settings-shortcut-conflict = 快捷键 { $shortcut } 跟别的指令有冲突。<br>若要继续，别的指令的快捷键会被清除。<br><br>冲突的指令有：<br>{ $conflictingAction }
action-settings-shortcut-conflict-continue = 继续
action-settings-shortcut-conflict-cancel = 取消

empty-room-page-title = 没有选中房间
empty-room-page-description = 当前没有选中的房间。

login-page-title = 登录
login-page-userid-prompt = 用户 id：
login-page-userid-input-placeholder = 例如： @foo:example.org
login-page-password-prompt = 密码：
login-page-login-button = 登录
login-page-close-button = 关闭
login-page-existing-sessions-prompt = 从已有会话中选一个：
login-page-alternative-password-login-prompt = 或者用用户 id 和密码启动新会话：
login-page-restore-session-button = 恢复会话
login-page-request-failed-prompt = 登录失败。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
login-page-discover-failed-enter-prompt = 不能检测此用户所在的服务器，或者服务器不可用。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。请手动输入服务器链接。
login-page-server-url-placeholder = 例如： https://example.org
login-page-server-url-prompt = 服务器链接（可选）：

session-load-failure-not-found = 找不到会话 { $sessionName }。
session-load-failure-format-unknown = 会话 { $sessionName } 包含不支持的格式。是由未来版本的 { -kt-app-name } 保存的吗？
session-load-failure-cannot-backup = 无法备份会话 { $sessionName }。
session-load-failure-lock-failed = 会话 { $sessionName } 正在被别的程序使用。
session-load-failure-cannot-open-file = 无法打开会话 { $sessionName } 的存档文件。
session-load-failure-deserialize-failed = 无法打开会话 { $sessionName }。是被损坏了或者是由未来版本的 { -kt-app-name } 保存的吗？

main-page-title = { -kt-app-name } - { $userId }
main-page-recent-tab-title = 最近
main-page-favourites-tab-title = 最爱
main-page-people-tab-title = 人们
main-page-rooms-tab-title = 房间
main-page-room-filter-prompt = 过滤房间...

room-list-view-room-item-title-name = { $name }
room-list-view-room-item-title-heroes = { $hero } { $otherNum ->
        [0] { "" }
        [1] 和 { $secondHero }
        *[other] 和别的 { $otherNum } 个人
    }
room-list-view-room-item-title-id = 未命名房间（{ $roomId }）
room-list-view-room-item-fav-action = 设为最爱
room-list-view-room-item-unread-indicator = 未读
room-list-view-room-item-unread-notification-count = { $count }
room-list-view-room-item-unread-notification-count-text = { $count } 条未读通知
room-tags-fav-action-notification = 把 { $name } 设为了最爱
room-tags-fav-action-notification-failed = 不能把 { $name } 设为最爱。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-tags-unfav-action-notification = 把 { $name } 从最爱中移除了
room-tags-unfav-action-notification-failed = 不能把 { $name } 从最爱中移除。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-tags-add-tag-action-notification = 把标签 { $tag } 添加到了 { $name }
room-tags-add-tag-action-notification-failed = 不能把标签 { $tag } 添加到 { $name }。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-tags-remove-tag-action-notification = 把标签 { $tag } 从 { $name } 移除了
room-tags-remove-tag-action-notification-failed = 不能把标签 { $tag } 从 { $name } 移除。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-list-view-room-item-more-action = 更多...
room-list-view-room-item-invited = （邀请）
room-list-view-room-item-left = （已离开）
room-list-view-room-item-tombstone = （已废弃）

room-settings-action = 房间设置...
room-settings-page-title = { $room } 的房间设置
room-settings-tags = 房间标签
room-settings-favourited = 设为最爱
room-settings-remove-tag = 移除标签
room-settings-add-tag = 添加标签
room-settings-members-action = 房间成员...
room-settings-banned-members-action = 被封禁的成员...
room-settings-enable-encryption-prompt-dialog-title = 启用加密
room-settings-enable-encryption-prompt-dialog-prompt = 一旦在本房间中启用了加密，就不能再禁用了。确定吗？
room-settings-enable-encryption-action = 启用加密
room-settings-encrypted = 本房间中的消息是端对端加密了的。
room-settings-not-encrypted = 本房间中的消息没有端对端加密。
room-settings-encryption-enabled-notification = 本房间中的加密已经启用。
room-settings-encryption-failed-to-enable-notification = 不能在本房间中启用加密。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-settings-name-missing = 这个房间没有名字。
room-settings-topic-missing = 这个房间没有话题。
room-settings-edit-name-action = 编辑房间名字
room-settings-edit-topic-action = 编辑话题
room-settings-save-name-action = 保存房间名字
room-settings-save-topic-action = 保存话题
room-settings-discard-name-action = 放弃房间名字
room-settings-discard-topic-action = 放弃话题
room-settings-set-name-failed-prompt = 不能设置房间名字。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-settings-set-topic-failed-prompt = 不能设置话题。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-sticker-packs-action = 贴纸包...
room-sticker-packs-page-title = { $room } 中的贴纸包
room-sticker-packs-use-action = 使用贴纸包
room-sticker-packs-use-failed = 设置使用的贴纸包失败。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-sticker-packs-page-add-action = 添加贴纸包
room-sticker-packs-page-add-popup-title = 添加贴纸包
room-sticker-packs-page-add-success = 已添加贴纸包。
room-sticker-packs-page-add-failed = 无法添加贴纸包。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-sticker-packs-page-add-popup-name-prompt = 贴纸包名称：
room-sticker-packs-page-add-popup-state-key-prompt = 状态键：

room-member-list-page-title = { $room } 的成员

room-leave-action = 离开房间
room-leave-confirm-popup-title = 离开房间
room-leave-confirm-popup-message = 你确定要离开这个房间吗？
room-leave-confirm-popup-confirm-action = 离开
room-leave-confirm-popup-cancel-action = 留下

room-forget-action = 忘记房间
room-forget-confirm-popup-title = 忘记房间
room-forget-confirm-popup-message = 你确定要忘记这个房间吗？
room-forget-confirm-popup-confirm-action = 忘记
room-forget-confirm-popup-cancel-action = 取消

send-message-box-input-placeholder = 在此输入您的讯息...
send-message-box-send = 发送
send-message-box-send-file = 发送文件
send-message-box-reply-to = 回复给
send-message-box-edit = 编辑
send-message-box-remove-reply-to-action = 移除回复关系
send-message-box-remove-replace-action = 取消编辑
send-message-box-stickers = 发送贴纸...
send-message-box-stickers-popup-title = 发送贴纸

sticker-picker-user-stickers = 我的贴纸
sticker-picker-room-sticker-pack-name = {$room} 中的 {$stateKey}
sticker-picker-room-default-sticker-pack-name = {$room} 中的默认包

room-timeline-load-more-action = 加载更多

room-invite-accept-action = 接受邀请
room-invite-reject-action = 拒绝邀请
room-invite-popup-title = 被邀请了
room-invite-popup-text = 你被邀请到这个房间了。
room-invite-popup-text-with-inviter = 你被 { $inviterName } 邀请到这个房间了。

room-pinned-events-action = { $count } 条置顶消息...
room-pinned-events-page-title = { $room } 的置顶消息

## 状态事件
## 通用参数：
## gender = 发送者的性别（male/female/neutral）
## stateKeyUser = state key 用户的名字
## stateKeyUserGender = state key 用户的性别
member-state-joined-room = 加入了房间。
member-state-changed-name-and-avatar = 修改了名字和头像。
member-state-changed-name = 修改了名字。
member-state-changed-avatar = 修改了头像。
member-state-invited = 把 { $stateKeyUser } 邀请到了本房间。
member-state-left = 离开了房间。
member-state-kicked = 踢出了 { $stateKeyUser }。
member-state-banned = 封禁了 { $stateKeyUser }。
member-state-unbanned = 解封了 { $stateKeyUser }。

state-room-created = 创建了房间。
state-room-name-changed = 把房间名字改成了 { $newName }。
state-room-topic-changed = 把房间话题改成了 { $newTopic }。
state-room-avatar-changed = 修改了房间头像。
state-room-pinned-events-changed = 修改了房间的置顶讯息。
state-room-alias-changed = 修改了房间的别名。
state-room-join-rules-changed = 修改了房间的加入规则。
state-room-power-levels-changed = 修改了房间的权限。
state-room-encryption-activated = 对本房间启用了加密。

event-message-image-sent = 发送了图片「{ $body }」。
event-message-sticker-sent = 发送了贴纸「{ $body }」。
event-summary-image-sent = 发送了图片「{ $body }」。
event-summary-sticker-sent = 发送了贴纸「{ $body }」。
event-message-file-sent = 发送了文件「{ $body }」。
event-message-video-sent = 发送了视频「{ $body }」。
event-summary-video-sent = 发送了视频「{ $body }」。
event-message-audio-sent = 发送了音频「{ $body }」。
event-message-audio-play-audio = 播放音频
event-sending = 发送中...
event-send-failed = 发送失败
event-resend = 重试发送这个事件
event-deleted = （已删除）
event-delete = 删除
event-delete-failed = 删除事件出错。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。
event-view-source = 查看源码...
event-view-history = 查看编辑历史...
event-source-popup-title = 事件源码
event-source-decrypted = 解密的事件源码
event-source-original = 原始的事件源码
event-history-popup-title = 事件编辑历史
event-reply-action = 回复
event-popup-action = 更多关于这个事件...
event-reacted-with = 回应了「{ $key }」
event-react-action = 回应...
event-react-popup-title = 回应一条消息
event-react-accept-action = 回应
event-react-cancel-action = 取消
event-react-with-prompt = 回应以：
event-react-button-description = 回应以 { $key }
event-edit-action = 编辑
event-encrypted = 这条消息已加密
event-msgtype-fallback-text = 未知的消息类型：{ $msgtype }
event-fallback-text = 未知事件：{ $type }
event-cannot-decrypt-text = （加密内容）
event-read-indicator-more = +{ $rest }
event-read-indicator-list-title = { $numUsers } 个用户已读
event-edited-indicator = （编辑过了）
event-pin-action = 在房间置顶
event-unpin-action = 从房间取消置顶
event-pin-confirmation-title = 在房间置顶
event-pin-confirmation = 确定要置顶这条消息吗？
event-pin-confirm-action = 置顶
event-pin-cancel-action = 不置顶
event-pin-success-prompt = 消息在房间置顶了。
event-pin-failed-prompt = 无法置顶消息。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
event-unpin-confirmation-title = 从房间取消置顶
event-unpin-confirmation = 确定要取消置顶这条消息吗？
event-unpin-confirm-action = 取消置顶
event-unpin-cancel-action = 不取消置顶
event-unpin-success-prompt = 消息从房间取消置顶了。
event-unpin-failed-prompt = 无法取消置顶消息。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。

media-file-menu-option-view = 查看
media-file-menu-option-save-as = 保存为
media-file-menu-add-sticker-action = 添加到贴纸...

add-sticker-popup-title = 添加到贴纸
add-sticker-popup-pack-prompt = 添加到：
add-sticker-popup-short-code-prompt = 短代码：
add-sticker-popup-short-code-exists-warning = 贴纸包里已经有这个短代码了。上面的贴纸会覆盖已有的。
add-sticker-popup-add-sticker-button = 添加
add-sticker-popup-cancel-button = 取消
add-sticker-popup-failed-prompt = 无法添加贴纸。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。

kazv-io-download-success-prompt = 下载成功
kazv-io-download-failure-prompt = 下载失败：{ $detail }
kazv-io-failure-detail-user-cancel = 用户已取消
kazv-io-failure-detail-network-error = 网络错误
kazv-io-failure-detail-open-file-error = 打开文件错误
kazv-io-failure-detail-write-file-error = 写入文件错误
kazv-io-failure-detail-hash-error = 哈息值校验失败
kazv-io-failure-detail-response-error = 接收到无效的响应
kazv-io-failure-detail-kazv-error = 未知错误，请将此报告为漏洞
kazv-io-upload-failure-prompt = 上传失败：{ $detail }
kazv-io-downloading-prompt = 正在下载：{ $fileName }
kazv-io-uploading-prompt = 正在上传：{ $fileName }
kazv-io-prompt-close = 好的
kazv-io-pause = 暂停
kazv-io-resume = 继续
kazv-io-cancel = 取消
kazv-io-progress = { $progress }/100

create-room-page-title = 创建房间
create-room-page-type-prompt = 房间类型：
create-room-page-type-public = 公开（每个人都可加入）
create-room-page-type-private = 私有（仅受邀请用户可加入）
create-room-page-type-direct = 私聊（与私有一样，但初始受邀请用户具有管理权限）
create-room-page-name-prompt = 房间名称（可选）：
create-room-page-name-placeholder = 无名
create-room-page-alias-prompt = 房间别名（可选）：
create-room-page-alias-placeholder = #foo:example.org
create-room-page-topic-prompt = 房间主题（可选）：
create-room-page-topic-placeholder = 无题
create-room-page-allow-federate-prompt = 允许别的服务器上的用户加入
create-room-page-action-create-room = 创建房间
create-room-page-success-prompt = 房间已创建。
create-room-page-failed-prompt = 无法创建房间。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。
create-room-invite-userids-prompt = 受邀请的用户的 Matrix id：
create-room-page-add-invite-prompt = 添加一个新的受邀请用户：
create-room-page-add-invite-placeholder = Matrix id，例如 @foo:example.org
create-room-page-action-add-invite = 添加
create-room-page-action-remove-invite = 移除
create-room-page-encrypted-prompt = 启用端对端加密

join-room-page-title = 加入房间
join-room-page-id-or-alias-prompt = 房间 id 或别名：
join-room-page-id-or-alias-placeholder = #foo:example.org 或 !abcdef:example.org
join-room-page-servers-prompt = 经由服务器（可选，用换行分割）：
join-room-page-servers-placeholder =
    example.org
    example.com
join-room-page-action-join-room = 加入房间
join-room-page-success-prompt = 成功加入房间 { $room }。
join-room-page-failed-prompt = 无法加入房间 { $room }。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。

leave-room-success-prompt = 成功离开房间 { $room }。
leave-room-failed-prompt = 无法离开房间 { $room }。错误代码：{ $errorCode }。错误讯息：{ $errorMsg }。

forget-room-success-prompt = 成功忘记房间 { $room }。
forget-room-failed-prompt = 无法忘记房间 { $room }。错误代码： { $errorCode }。错误讯息: { $errorMsg }。

device-trust-level-unseen = 未曾见过
device-trust-level-seen = 见过
device-trust-level-verified = 已验证
device-trust-level-blocked = 已屏蔽
device-set-trust-level = 设置信任等级...
device-set-trust-level-dialog-title = 设置信任等级
device-set-trust-level-dialog-name-label = 设备名：{ $name }
device-set-trust-level-dialog-id-label = 设备id：{ $id }
device-set-trust-level-dialog-ed25519-key-label = Ed25519公钥：{ $key }
device-set-trust-level-dialog-curve25519-key-label = Curve25519公钥：{ $key }
device-set-trust-level-dialog-save = 保存
device-set-trust-level-dialog-cancel = 取消

settings-page-title = 设置
settings-save = 保存设置
settings-profile-load-failed-prompt = 无法加载用户资料。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。
settings-profile-change-avatar = 改变头像...
settings-profile-display-name = 显示名：
settings-profile-save-failed-prompt = 无法保存用户资料。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。
settings-cache-directory = 缓存目录：

typing-indicator = { $typingUser } { $otherNum ->
        [0] 正在输入...
        [1] 和 { $secondTypingUser } 正在输入...
        *[other] 和另外 { $otherNum } 人正在输入...
    }

notification-message = <b>{ $user }：</b> { $message }
notification-message-no-content = <b>{ $user }</b> 给你发了一条讯息。
notification-open = 打开

user-page-power-level = 权限等级：{ $powerLevel }
user-page-edit-power-level-action = 编辑
user-page-save-power-level-action = 保存
user-page-discard-power-level-action = 丢弃
user-page-set-power-level-failed-prompt = 无法设置权限等级。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。
user-page-kick-user-action = 踢出
user-page-kick-user-confirm-dialog-title = 踢出用户
user-page-kick-user-confirm-dialog-content = 确定要把 { $name }（{ $userId }）踢出 { $roomName } 吗？
user-page-kick-user-reason-prompt = 原因（可选）：
user-page-kick-user-failed-prompt = 无法踢出用户。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。

user-page-ban-user-action = 封禁
user-page-ban-user-confirm-dialog-title = 封禁用户
user-page-ban-user-confirm-dialog-content = 确定要在 { $roomName } 中封禁 { $name }（{ $userId }）吗？
user-page-ban-user-reason-prompt = 原因（可选）：
user-page-ban-user-failed-prompt = 无法封禁用户。错误码：{ $error }。错误讯息：{ $errorMsg }。
user-page-unban-user-action = 解禁
user-page-unban-user-failed-prompt = 无法解禁用户。错误码：{ $error }。错误讯息：{ $errorMsg }。
user-page-overrided-name-placeholder = 自定义显示名...
user-page-save-name-override-action = 保存
user-page-update-name-override-failed-prompt = 无法设置自定义显示名。错误码：{ $error }。错误讯息：{ $errorMsg }。
user-page-self-name-prompt = 自己在房间里的显示名：
user-page-self-name-placeholder = 自己在房间里的显示名...
user-page-save-self-name-action = 保存
user-page-update-self-name-failed-prompt = 无法设置自己在房间里的显示名。错误码：{ $error }。错误讯息：{ $errorMsg }。

room-invite-page-title = 邀请用户到 { $room }
room-invite-page-invite-failed-prompt = 无法邀请用户。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。
room-invite-invitee-matrix-id-placeholder = 用户的 Matrix id，比如 @foo:example.org
room-invite-invitee-matrix-id-prompt = 要邀请的用户：
room-invite-page-invite-button = 邀请
room-invite-action = 邀请到房间...

confirm-upload-popup-title = 确认上传
confirm-upload-popup-prompt = 即将上传「{ $file }」（{ $current } / { $total }）。
confirm-upload-popup-prompt-single-file = 即将上传「{ $file }」。
confirm-upload-popup-accept-button = 上传
confirm-upload-popup-cancel-button = 取消

action-cut = 剪切
action-copy = 复制
action-paste = 粘贴
action-undo = 撤销
action-redo = 重做
action-delete = 删除
action-select-all = 全选

logout-failed-prompt = 登出失败，请检查您的网络后重试。错误码：{ $errorCode }。错误讯息：{ $errorMsg }。

confirm-logout-popup-title = 确认登出
confirm-logout-popup-prompt = 你确定要登出吗？
confirm-logout-popup-accept-button = 是
confirm-logout-popup-cancel-button = 取消

emoji-smileys-and-emotion = 笑容与情绪
emoji-people-and-body = 人与身体
emoji-animals-and-nature = 动物与自然
emoji-food-and-drink = 食物与饮料
emoji-travel-and-places = 旅行与地点
emoji-activities = 活动
emoji-objects = 物体
emoji-symbols = 符号
emoji-flags = 旗帜

confirm-deletion-popup-title = 删除事件
confirm-deletion-popup-message = 你确定要删除这个事件吗？
confirm-deletion-popup-confirm-action = 删除
confirm-deletion-popup-cancel-action = 取消
