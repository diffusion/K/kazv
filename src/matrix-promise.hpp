/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <memory>

#include <QObject>
#include <QQmlEngine>

#include <promise-interface.hpp>
#include <sdk.hpp>
Q_MOC_INCLUDE(<QJsonValue>)
Q_MOC_INCLUDE(<QJSValue>)

class QJsonValue;
class QJSValue;

class MatrixPromise : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_UNCREATABLE("")

    explicit MatrixPromise(QObject *parent = 0);
public:
    explicit MatrixPromise(Kazv::SingleTypePromise<Kazv::EffectStatus> promise, QObject *parent = 0);
#if KAZV_KF_QT_MAJOR_VERSION == 6
    MatrixPromise(const QJSValue &) = delete;
#endif
    ~MatrixPromise();

    static MatrixPromise *createResolved(bool isSuccess, const QJsonValue &data, QObject *parent = 0);

Q_SIGNALS:
    /**
     * Emitted when the Promise is resolved.
     */
    void resolved(bool isSuccess, const QJsonValue &data);

    /**
     * Emitted when the Promise is resolved successfully.
     */
    void succeeded(const QJsonValue &data);

    /**
     * Emitted when the Promise is resolved but failed.
     */
    void failed(const QJsonValue &data);

private:
    struct Private;
    std::unique_ptr<Private> m_d;
};
