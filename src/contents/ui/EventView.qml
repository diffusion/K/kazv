/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import Qt.labs.qmlmodels 1.0

import org.kde.kirigami 2.13 as Kirigami

import 'event-types' as Types

Item {
  id: eventView
  property var event
  property var prevEvent
  property var prevEventType: prevEvent ? getMessageType(prevEvent) : 'ignore'
  property var sender
  property var stateKeyUser
  property var isSelected: false
  property var displayTime

  property var messageType: getMessageType(event)

  property var iconSize: Kirigami.Units.iconSizes.large
  property var inlineBadgeSize: Kirigami.Units.iconSizes.smallMedium
  property var senderNameSize: Kirigami.Units.gridUnit * 1.2

  property var isGapped: false
  property var compactMode: false

  property var calculatedHeight: (loader.item ? loader.item.implicitHeight : 0)

  height: calculatedHeight
  implicitHeight: calculatedHeight
  Layout.minimumHeight: calculatedHeight
  Layout.preferredHeight: calculatedHeight

  Component {
    id: textEV
    Types.Text {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: emoteEV
    Types.Emote {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: noticeEV
    Types.Notice {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: stateEV
    Types.State {
      event: eventView.event
      sender: eventView.sender
      stateKeyUser: eventView.stateKeyUser
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: imageEV
    Types.Image {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: fileEV
    Types.File {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: videoEV
    Types.Video {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: audioEV
    Types.Audio {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: unknownEV
    Types.Fallback {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: redactedEV
    Types.Redacted {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: reactionEV
    Types.Reaction {
      event: eventView.event
      sender: eventView.sender
      isSelected: eventView.isSelected
    }
  }

  Component {
    id: ignoreEV
    Item {
    }
  }

  Loader {
    sourceComponent: getSource(messageType)
    id: loader
    anchors.left: eventView.left
    anchors.right: eventView.right
    anchors.top: eventView.top
  }

  function getSource(t) {
    switch (t) {
    case 'text':
      return textEV;

    case 'emote':
      return emoteEV;

    case 'notice':
      return noticeEV;

    case 'state':
      return stateEV;

    case 'image':
      return imageEV;

    case 'file':
      return fileEV;

    case 'video':
      return videoEV;

    case 'audio':
      return audioEV;

    case 'redacted':
      return redactedEV;

    case 'reaction':
      return reactionEV;

    case 'ignore':
      return ignoreEV;

    default:
      return unknownEV;
    }
  }

  function getMessageType(e) {
    if (e.redacted) {
      return 'redacted';
    }
    if (e.isState) {
      return 'state';
    }
    // ignore edits of other events
    if (e.relationType === 'm.replace') {
      return 'ignore';
    }
    switch (e.type) {
    case 'm.room.message':
      switch (e.content.msgtype) {
      case 'm.text':
        return 'text';

      case 'm.emote':
        return 'emote';

      case 'm.notice':
        return 'notice';

      case 'm.image':
        return 'image';

      case 'm.file':
        return 'file';

      case 'm.audio':
        return 'audio';

      case 'm.video':
        return 'video';

      case 'm.location':
        return 'location';

      default:
        return 'unknown';
      }

    case 'm.sticker':
      return 'image';

    case 'm.room.redaction':
      return 'ignore';

    case 'm.reaction':
      // Only display a reaction if it is a failed local echo
      // (otherwise it will be stuck forever and we have no way to delete it)
      return e.isFailed ? 'reaction' : 'ignore';

    default:
      return 'unknown';
    }
  }

  function paginateBack() {
    paginateBackRequested(event.eventId);
  }

  function reactWith(reactionText) {
    room.sendReaction(reactionText, event.eventId);
  }

  onIsGappedChanged: maybePaginateBack()
  onEventChanged: maybePaginateBack()
  Component.onCompleted: maybePaginateBack()
  Connections {
    target: eventView.event

    function onEventIdChanged() {
      eventView.maybePaginateBack();
    }
  }

  function maybePaginateBack() {
    if (isGapped) {
      paginateBack();
    }
  }
}
