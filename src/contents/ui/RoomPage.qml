/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv
import 'matrix-helpers.js' as Helpers

Kazv.ScrollablePageAdapter {
  id: roomPage
  property string roomId: ''
  property var room: sdkVars.roomList.room(roomId)
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: roomPage.room
  }
  property var roomTimeline: room.timeline()
  property var pinnedEvents: room.pinnedEventsTimeline()
  property var lastReceiptableEventId: getLastReceiptableEventId(roomTimeline, roomTimeline.count)
  property var paginationRequests: ({})
  property var refreshRoomStateRequest: null

  signal mentionUserRequested(string userId)
  signal replaceDraftRequested(string newDraft)
  signal paginateBackRequested(string eventId)
  signal viewEventHistoryRequested(string eventId)

  title: roomNameProvider.name

  property var isInvite: room.membership === MK.MatrixRoom.Invite
  property var isJoin: room.membership === MK.MatrixRoom.Join

  property var eventHistoryPopupComp: Component {
    Kazv.SelfDestroyableOverlaySheet {
      id: historyPopup
      property var eventId
      property var event: room.messageById(eventId)
      shouldSelfDestroy: true
      title: l10n.get('event-history-popup-title')

      Kazv.EventHistoryView {
        Layout.preferredWidth: Math.min(Kirigami.Units.gridUnit * 40, Window.width)
        history: historyPopup.event.history()
      }
    }
  }

  contextualActions: [
    Kirigami.Action {
      id: acceptInviteAction
      icon.name: 'checkmark'
      text: l10n.get('room-invite-accept-action')
      visible: isInvite
      onTriggered: joinRoomHandler.call()
    },
    Kirigami.Action {
      id: rejectInviteAction
      icon.name: 'im-ban-kick-user'
      text: l10n.get('room-invite-reject-action')
      visible: isInvite
      onTriggered: leaveRoomHandler.call()
    },
    Kirigami.Action {
      id: pinnedEventsAction
      objectName: 'pinnedEventsAction'
      icon.name: 'pin'
      visible: roomPage.pinnedEvents.count
      enabled: roomPage.pinnedEvents.count
      text: l10n.get('room-pinned-events-action', { count: roomPage.pinnedEvents.count })
      onTriggered: activateRoomPinnedEventsPage(room)
    },
    Kirigami.Action {
      id: roomSettingsAction
      icon.name: 'settings-configure'
      text: l10n.get('room-settings-action')
      onTriggered: activateRoomSettingsPage(room)
    }
  ]

  property var inviteOverlay: Kirigami.OverlaySheet {
    id: inviteOverlay
    title: l10n.get('room-invite-popup-title')
    parent: roomPage.overlay

    property var self: room.member(matrixSdk.userId)
    property var inviteEvent: self.toEvent()
    property var inviter: room.member(inviteEvent.sender)
    property var inviterName: Helpers.userNameWithId(inviter, l10n)

    ColumnLayout {
      Label {
        text: {
          inviteOverlay.inviterName
            ? l10n.get('room-invite-popup-text-with-inviter', { inviterName: inviteOverlay.inviterName })
            : l10n.get('room-invite-popup-text')
        }
      }

      RowLayout {
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter
        Button {
          action: acceptInviteAction
        }

        Button {
          action: rejectInviteAction
        }
      }
    }
  }

  Component.onCompleted: {
    if (isInvite) {
      inviteOverlay.open();
    }
  }

  RoomTimelineView {
    timeline: roomPage.roomTimeline
  }

  onRoomChanged: {
    kazvIOManager.deleteModelIfEmpty(roomId)
    uploadList.model = kazvIOManager.getUploadJobs(roomId)
  }

  footer: Item {
    height: childrenRect.height
    width: parent.width

    ListView {
      id: uploadList
      width: parent.width
      height: contentHeight
      model: kazvIOManager.getUploadJobs(roomId)
      delegate: Kazv.KazvIOMenu {
        width: parent.width
        jobId: roomId
        isUpload: true
      }
    }

    Kazv.TypingIndicator {
      id: typingIndicator
      typingUsers: roomPage.room.typingUsers()
      width: parent.width
      anchors.top: uploadList.bottom
    }

    Kazv.SendMessageBox {
      id: sendMessageBox
      objectName: 'sendMessageBox'
      width: parent.width
      room: roomPage.room
      anchors.top: typingIndicator.bottom
      enabled: isJoin
    }
  }

  function setDraftRelation(relType, eventId) {
    sendMessageBox.draftRelType = relType;
    sendMessageBox.draftRelatedTo = eventId;
  }

  property var joinRoomHandler: Kazv.AsyncHandler {
    trigger: () => matrixSdk.joinRoom(roomId, [])
    onResolved: {
      if (success) {
        showPassiveNotification(l10n.get('join-room-page-success-prompt', { room: roomId }));
        inviteOverlay.close();
      } else {
        showPassiveNotification(l10n.get('join-room-page-failed-prompt', { room: roomId, errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  property var leaveRoomHandler: Kazv.AsyncHandler {
    trigger: () => room.leaveRoom()
    onResolved: {
      if (success) {
        showPassiveNotification(l10n.get('leave-room-success-prompt', { room: roomId }));
        inviteOverlay.close();
      } else {
        showPassiveNotification(l10n.get('leave-room-failed-prompt', { room: roomId, errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  onMentionUserRequested: (userId) => {
    sendMessageBox.mentionUser(userId);
    sendMessageBox.focusInput();
  }

  onReplaceDraftRequested: (newDraft) => {
    sendMessageBox.replaceDraft(newDraft);
  }

  function getLastReceiptableEventId(timeline, timelineCount) {
    for (let i = 0; i < timelineCount; ++i) {
      const event = timeline.at(i);
      if (!event.isLocalEcho && event.sender !== matrixSdk.userId) {
        return event.eventId;
      }
    }
  }

  onLastReceiptableEventIdChanged: maybePostReadReceipt()

  Window.onActiveChanged: maybePostReadReceipt()

  onVisibleChanged: maybePostReadReceipt()

  function maybePostReadReceipt() {
    if (!Window.active || !roomPage.visible) {
      return;
    }

    const eventId = roomPage.lastReceiptableEventId;
    if (!eventId) {
      return;
    }

    const event = roomPage.room.messageById(eventId);
    const readByMe = Helpers.isEventReadBy(event, matrixSdk.userId);
    if (!readByMe) {
      roomPage.room.postReadReceipt(eventId);
    }
  }

  onPaginateBackRequested: (eventId) => {
    if (!paginationRequests[eventId]) {
      console.debug('paginating back from', eventId);
      paginationRequests[eventId] = room.paginateBackFrom(eventId);
      paginationRequests[eventId].resolved.connect(() => {
        console.debug('finished paginating back from', eventId);
        delete paginationRequests[eventId];
      });
    } else {
      console.debug('pagination request of', eventId, 'is already under way');
    }
  }

  onViewEventHistoryRequested: (eventId) => {
    eventHistoryPopupComp.createObject(applicationWindow().overlay, { eventId }).open();
  }
}
