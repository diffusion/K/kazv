/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami

import '.' as Types

Types.TextTemplate {
  id: upper
  objectName: 'redactedEvent'

  text: l10n.get('event-deleted')

  Kirigami.Icon {
    source: 'edit-delete-symbolic'
    Layout.preferredHeight: inlineBadgeSize
    Layout.preferredWidth: inlineBadgeSize
  }
}
