/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

Kirigami.OverlaySheet {
  id: confirmUploadPopup
  property var fileUrls: []
  property var total: fileUrls.length
  property int currentIndex: 0
  property var currentFile: fileUrls[currentIndex] || ''
  property var currentFileBasename: (new URL(currentFile)).toString().split('/').slice(-1)[0]

  title: l10n.get('confirm-upload-popup-title')

  signal uploadRequested(var fileUrl)

  function call(urls) {
    confirmUploadPopup.fileUrls = [...urls];
    confirmUploadPopup.currentIndex = 0;
    confirmUploadPopup.open();
  }

  ColumnLayout {
    Layout.preferredWidth: Kirigami.Units.gridUnit * 40
    Label {
      text: (
        confirmUploadPopup.total === 1
          ? l10n.get('confirm-upload-popup-prompt-single-file', {
            file: confirmUploadPopup.currentFileBasename,
          }) : l10n.get('confirm-upload-popup-prompt', {
            file: confirmUploadPopup.currentFileBasename,
            current: confirmUploadPopup.currentIndex + 1,
            total: confirmUploadPopup.total,
          })
      )
      wrapMode: Text.Wrap
      Layout.preferredWidth: Kirigami.Units.gridUnit * 10
      Layout.fillWidth: true
    }

    Image {
      id: image
      objectName: 'imagePreview'
      visible: confirmUploadPopup.currentFile && MK.KazvUtil.mimeTypeForUrl(confirmUploadPopup.currentFile).startsWith('image/')
      source: confirmUploadPopup.currentFile
      Layout.maximumHeight: Kirigami.Units.gridUnit * 5
      Layout.preferredWidth: Kirigami.Units.gridUnit * 10
      Layout.fillWidth: true
      fillMode: Image.PreserveAspectFit
    }

    RowLayout {
      Layout.fillWidth: true
      Layout.alignment: Qt.AlignRight
      Button {
        objectName: 'acceptButton'
        text: l10n.get('confirm-upload-popup-accept-button')
        onClicked: {
          uploadRequested(confirmUploadPopup.currentFile);
          if (confirmUploadPopup.currentIndex + 1 < confirmUploadPopup.total) {
            ++confirmUploadPopup.currentIndex;
          } else {
            confirmUploadPopup.close();
          }
        }
      }

      Button {
        objectName: 'cancelButton'
        text: l10n.get('confirm-upload-popup-cancel-button')
        onClicked: confirmUploadPopup.close();
      }
    }
  }
}
