/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 nannanko <nannanko@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import moe.kazv.mxc.kazv 0.0 as MK

import QtQml 2.15
import QtQuick 2.15

/**
 * Used to handle files described in event, such as caching(if autoCache is true) and download.
 * It is either used for caching or downloading. These two operations cannot be performed in the same FileHandler.
 */
QtObject {
  id: fileHandler

  required property var eventContent
  /**
   * If autoCache is true, the file is automatically cached(thumbnail is downloaded if exists).
   */
  required property bool autoCache
  required property var kazvIOManager
  required property var matrixSdk

  /**
   * Used to download or caching.
   * mxcUri, sha256, key, iv will be setted automatically
   * according to whether the file is encrypted or not.
   */
  property var mxcUri: ""
  property var url: mxcUri ? fileHandler.matrixSdk.mxcUriToHttp(fileHandler.mxcUri) : ""
  property var mediaId: mxcUri && fileHandler.getMediaId(fileHandler.mxcUri)
  property var sha256: ""
  property var key: ""
  property var iv: ""

  property var kazvIOJob
  readonly property url localFile: kazvIOJob ? '' : cachedFile
  property real percent: fileHandler.kazvIOJob ? fileHandler.kazvIOJob.progress : 0.0  // from 0 to 1
  property var result
  property bool success: fileHandler.result == MK.KazvIOBaseJob.NoError
  /**
   * This attribute is only used after result is set (that is, after signal resultChanged is emitted),
   * and only when success is true, this attribute has a correct and valid value.
   */
  property url cachedFile

  property var fileInfo: "info" in eventContent ? eventContent.info : {}

  property var encryptedFile: "file" in eventContent ? eventContent.file : {}
  property var encryptedFileMxcUri: "url" in encryptedFile ? encryptedFile.url : ""
  property var encryptedFileSha256: "hashes" in encryptedFile && "sha256" in encryptedFile.hashes ? encryptedFile.hashes.sha256 : ""
  // property var encryptedFileAlg: "key" in encryptedFile && "alg" in encryptedFile.key ? encryptedFile.key.alg : ""
  property var encryptedFileKey: "key" in encryptedFile && "k" in encryptedFile.key ? encryptedFile.key.k : ""
  property var encryptedFileIv: "iv" in encryptedFile ? encryptedFile.iv : ""

  property var encryptedThumbnail: "thumbnail_file" in fileInfo ? fileInfo.thumbnail_file : {}
  property var encryptedThumbnailMxcUri: "url" in encryptedThumbnail ? encryptedThumbnail.url : ""
  property var encryptedThumbnailSha256: "hashes" in encryptedThumbnail && "sha256" in encryptedThumbnail.hashes ? encryptedThumbnail.hashes.sha256 : ""
  // property var encryptedFileAlg: "key" in encryptedThumbnail && "alg" in encryptedThumbnail.key ? encryptedThumbnail.key.alg : ""
  property var encryptedThumbnailKey: "key" in encryptedThumbnail && "k" in encryptedThumbnail.key ? encryptedThumbnail.key.k : ""
  property var encryptedThumbnailIv: "iv" in encryptedThumbnail ? encryptedThumbnail.iv : ""

  property var unencryptedFileMxcUri: "url" in eventContent ? eventContent.url : ""

  property var unencryptedThumbnailMxcUri: "thumbnail_url" in fileInfo ? fileInfo.thumbnail_url : ""

  /**
   * Emit when start download, not emit when start cache.
   * Can be used to indicate progress bar visible
   */
  signal startDownload

  function getMediaId(mxcUri) {
    const temp = mxcUri.toString().split('/')
    return temp[2] + '_' + temp[3]
  }

  function downloadFile(saveFileUrl) {
    fileHandler.kazvIOManager.startNewDownloadJob(fileHandler.url,saveFileUrl,
                                                  fileHandler.mediaId,
                                                  fileHandler.sha256,
                                                  fileHandler.key,
                                                  fileHandler.iv)
    fileHandler.updateKazvIOJob(fileHandler.mediaId)

    fileHandler.startDownload()
  }

  function cacheFile() {
    fileHandler.cachedFile = fileHandler.kazvIOManager.cacheFile(fileHandler.url,
                                                                 fileHandler.mediaId,
                                                                 fileHandler.sha256,
                                                                 fileHandler.key,
                                                                 fileHandler.iv)
    fileHandler.updateKazvIOJob(fileHandler.mediaId)
  }

  function updateKazvIOJob(mediaId) {
    fileHandler.kazvIOJob = null
    if (autoCache) {
      fileHandler.kazvIOJob = fileHandler.kazvIOManager.getCacheJob(fileHandler.mediaId)
    } else {
      fileHandler.kazvIOJob = fileHandler.kazvIOManager.getDownloadJob(fileHandler.mediaId)
    }
    if (kazvIOJob) {
      fileHandler.kazvIOJob.result.connect(function (ec) {
        fileHandler.result = ec
      })
    }
  }

  Component.onCompleted: {
    // Check if there is an encryted file or unencrypted file
    if (encryptedFileMxcUri &&
        encryptedFileSha256 &&
        encryptedFileKey &&
        encryptedFileIv) {
      fileHandler.mxcUri = encryptedFileMxcUri
      fileHandler.sha256 = encryptedFileSha256
      fileHandler.key = encryptedFileKey
      fileHandler.iv = encryptedFileIv
    } else if (unencryptedFileMxcUri) {
      fileHandler.mxcUri = unencryptedFileMxcUri
    } else {
      // The event content is incorrect or the FileHandler.qml has a bug
      return
    }

    /**
     * Thumbnail will only be downloaded in the cache
     **/
    if (fileHandler.autoCache) {
      // Check if there is an encrypted thumbnail or unencrypted thumbnail
      if (encryptedThumbnailMxcUri &&
          encryptedThumbnailSha256 &&
          encryptedThumbnailKey &&
          encryptedThumbnailIv) {
        fileHandler.mxcUri = encryptedThumbnailMxcUri
        fileHandler.sha256 = encryptedThumbnailSha256
        fileHandler.key = encryptedThumbnailKey
        fileHandler.iv = encryptedThumbnailIv
      } else if (unencryptedThumbnailMxcUri) {
        fileHandler.mxcUri = unencryptedThumbnailMxcUri
      }
      fileHandler.cacheFile()
    }

    fileHandler.updateKazvIOJob(fileHandler.mediaId)
    fileHandler.onSuccessChanged.connect(function () {
      fileHandler.kazvIOManager.deleteDownloadJob(mediaId)
    })
  }
}
