/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import org.kde.kirigami 2.20 as Kirigami
import QtQuick.Controls 2.15
import moe.kazv.mxc.kazv 0.0 as MK

import '.' as Kazv

QtObject {
  property var pageStack
  property var sdkVars
  property var main

  function switchToRoom(roomId) {
    const nextPage = pageStack.get(pageStack.currentIndex + 1)
    if (nextPage &&
        nextPage instanceof Kazv.RoomPage &&
        nextPage.roomId === roomId) {
      pageStack.goForward();
    } else {
      sdkVars.currentRoomId = '';
      sdkVars.currentRoomId = roomId;
    }
  }

  function activateRoomPage() {
    // Go back to the first page. This ensures that the room page
    // will always be the second page.
    while (pageStack.currentIndex > 0) {
      pageStack.goBack();
    }
    pageStack.push(Qt.resolvedUrl("RoomPage.qml"), {
      roomId: sdkVars.currentRoomId,
    });
  }

  property var sdkVarsConn: Connections {
    target: sdkVars

    function onCurrentRoomIdChanged() {
      if (sdkVars.currentRoomId.length) {
        activateRoomPage();
      }
    }
  }

  property var mainConn: Connections {
    target: main
    function onSwitchToRoomRequested(roomId) {
      switchToRoom(roomId);
    }
  }
}
