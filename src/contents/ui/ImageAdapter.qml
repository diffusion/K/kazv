/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import '.' as Kazv

Image {
  id: image
  property string mxcUri
  readonly property var manager: kazvIOManager
  readonly property var sdk: matrixSdk
  source: fileHandler?.localFile || ''

  property var fileHandlerComp: Component {
    Kazv.FileHandler {
      autoCache: true
      kazvIOManager: image.manager
      matrixSdk: image.sdk
    }
  }
  property var fileHandler: null

  onMxcUriChanged: {
    if (fileHandler) {
      fileHandler.destroy();
      fileHandler = null;
    }

    fileHandler = fileHandlerComp.createObject(image, {
      eventContent: ({ url: image.mxcUri }),
    });
  }
}
