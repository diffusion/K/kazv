/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2022-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.1
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import '.' as Kazv

Kazv.ClosableScrollablePage {
  id: joinRoomPage
  title: l10n.get('join-room-page-title')

  ButtonGroup {
    id: visibilityButtons
  }

  ColumnLayout {
    width: parent.width
    spacing: Kirigami.Units.largeSpacing

    GridLayout {
      columns: 2

      Label {
        text: l10n.get('join-room-page-id-or-alias-prompt')
      }
      TextField {
        id: idOrAlias
        objectName: 'idOrAliasInput'
        placeholderText: l10n.get('join-room-page-id-or-alias-placeholder')
        Layout.fillWidth: true
      }

      Label {
        text: l10n.get('join-room-page-servers-prompt')
      }
      TextArea {
        id: servers
        objectName: 'serversInput'
        placeholderText: l10n.get('join-room-page-servers-placeholder')
        Layout.fillWidth: true
      }
    }

    Button {
      objectName: 'joinRoomButton'
      text: l10n.get('join-room-page-action-join-room')
      onClicked: joinRoomPage.joinRoom()
    }
  }

  property var joinRoomHandler: Kazv.AsyncHandler {
    property var serverNames: servers.text.split('\n').map(k => k.trim()).filter(k => k)
    property var room: idOrAlias.text
    trigger: () => matrixSdk.joinRoom(room, serverNames)
    onResolved: {
      if (success) {
        showPassiveNotification(l10n.get('join-room-page-success-prompt', { room }));
        pageStack.removePage(joinRoomPage);
      } else {
        showPassiveNotification(l10n.get('join-room-page-failed-prompt', { room, errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  function joinRoom()
  {
    joinRoomHandler.call();
  }
}
