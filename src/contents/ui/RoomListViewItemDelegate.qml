/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.2
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

import 'matrix-helpers.js' as Helpers
import 'room-settings' as RoomSettings

import '.' as Kazv

Kirigami.SwipeListItem {
  id: upper
  property var item
  property var iconSize: 1
  property var isFavourite: item.tagIds.includes('m.favourite')
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: upper.item
  }
  property var roomDisplayName: roomNameProvider.name
  property var roomTimeline: item.timeline()
  property var lastUnreadMessage: getLastUnreadMessage(roomTimeline, roomTimeline.count, item)
  property var lastUnreadMessageReaders: lastUnreadMessage ? lastUnreadMessage.readers() : null
  // The readers count is to make hasUnreadMessages prop reactive
  // against our newly-posted receipts
  property var hasUnreadMessages: getHasUnreadMessages(item, roomTimeline, roomTimeline.count, lastUnreadMessageReaders ? lastUnreadMessageReaders.count : 0)

  onClicked: switchToRoomRequested(item.roomId)
  checkable: true
  checked: sdkVars.currentRoomId == item.roomId
  autoExclusive: true
  Kirigami.Theme.colorSet: !checked ? Kirigami.Theme.View : Kirigami.Theme.Selection
  Kirigami.Theme.inherit: false
  background: Rectangle {
    anchors.fill: parent
    color: hovered ? Kirigami.Theme.hoverColor : Kirigami.Theme.backgroundColor
  }

  contentItem: RowLayout {
    Kazv.AvatarAdapter {
      mxcUri: item.roomOrHeroAvatarMxcUri
      name: Helpers.roomAvatarPlaceholderName(item)
    }

    Label {
      objectName: 'tombstoneIndicator'
      visible: item.tombstoned === true
      text: l10n.get('room-list-view-room-item-tombstone')
      background: Rectangle {
        color: Kirigami.Theme.negativeBackgroundColor
      }
      color: Kirigami.Theme.negativeTextColor
    }

    Label {
      objectName: 'inviteIndicator'
      visible: item.membership === MK.MatrixRoom.Invite
      text: l10n.get('room-list-view-room-item-invited')
      background: Rectangle {
        color: Kirigami.Theme.activeBackgroundColor
      }
      color: Kirigami.Theme.activeTextColor
    }

    Label {
      objectName: 'leaveIndicator'
      visible: item.membership === MK.MatrixRoom.Leave
      text: l10n.get('room-list-view-room-item-left')
      color: Kirigami.Theme.disabledTextColor
    }

    Label {
      objectName: 'roomDisplayNameLabel'
      text: roomDisplayName
      Layout.fillWidth: true
      elide: Text.ElideRight
      color: item.membership === MK.MatrixRoom.Leave ? Kirigami.Theme.disabledTextColor : Kirigami.Theme.textColor
    }

    Rectangle {
      objectName: 'unreadNotificationCount'
      Layout.alignment: Qt.AlignRight
      Layout.rightMargin: Kirigami.Units.largeSpacing
      Layout.preferredWidth: Math.max(unreadNotificationCountLabel.implicitWidth, Kirigami.Units.gridUnit)
      Layout.preferredHeight: Kirigami.Units.gridUnit
      radius: Kirigami.Units.gridUnit / 2
      color: Kirigami.Theme.activeBackgroundColor
      visible: !!item.unreadNotificationCount

      RowLayout {
        anchors.fill: parent
        Label {
          id: unreadNotificationCountLabel
          Layout.alignment: Qt.AlignHCenter
          color: Kirigami.Theme.activeTextColor
          text: l10n.get('room-list-view-room-item-unread-notification-count', { count: item.unreadNotificationCount })
          Accessible.ignored: true
        }
      }
      Accessible.role: Accessible.Indicator
      Accessible.name: l10n.get('room-list-view-room-item-unread-notification-count-text', { count: item.unreadNotificationCount })
    }

    Rectangle {
      objectName: 'unreadIndicator'
      Layout.alignment: Qt.AlignRight
      Layout.rightMargin: Kirigami.Units.largeSpacing
      Layout.preferredWidth: Kirigami.Units.iconSizes.small
      Layout.preferredHeight: Kirigami.Units.iconSizes.small
      radius: Kirigami.Units.iconSizes.small / 2
      color: Kirigami.Theme.activeTextColor
      visible: !item.unreadNotificationCount && hasUnreadMessages
      Accessible.role: Accessible.Indicator
      Accessible.name: l10n.get('room-list-view-room-item-unread-indicator')
    }
  }
  actions: [
    Kirigami.Action {
      text: l10n.get("room-list-view-room-item-fav-action")
      checkable: true
      checked: isFavourite
      icon.name: isFavourite ? 'starred-symbolic' : 'non-starred-symbolic'
      onTriggered: toggleFavourite()
    },
    Kirigami.Action {
      text: l10n.get('room-list-view-room-item-more-action')
      icon.name: 'view-more-horizontal-symbolic'
      onTriggered: activateRoomSettingsPage(item)
    }
  ]

  function toggleFavourite() {
    favouriteToggler.toggleTag('m.favourite');
  }

  property var favouriteToggler: RoomSettings.RoomTagHandler {
    room: item
  }

  function getHasUnreadMessages(room, timeline, timelineCount, readersCount) {
    return !!room.unreadNotificationCount || !!getLastUnreadMessage(timeline, timelineCount, room);
  }

  function getLastUnreadMessage(timeline, timelineCount, room) {
    for (let i = 0; i < timelineCount; ++i) {
      const event = timeline.at(i);
      if (event.isLocalEcho) {
        // Skip local echoes
      } else if (room.localReadMarker === event.eventId) {
        // This message is read locally
        return undefined;
      } else if (event.sender === matrixSdk.userId) {
        // Own message here
        return undefined;
      } else if (Helpers.isEventReadBy(event, matrixSdk.userId)) {
        // This message is read
        return undefined;
      } else {
        // The latest message is not read and not sent by the current user
        return event;
      }
    }
  }
}
