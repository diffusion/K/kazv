/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2022 Tusooa Zhu <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import moe.kazv.mxc.kazvshortcuts 0.0 as MKShortcuts

Item {
  id: input
  property var past: []
  property var current: ''
  readonly property alias editMode: input.focus

  property var partialResult: toSeqString(past.concat([current]))
  signal finalResult(var sequence)

  focus: false

  Timer {
    id: resultTimer
    interval: 1000
    onTriggered: {
      input.finalResult(toSeqString(input.past));
      input.focus = false;
    }
  }

  Keys.onPressed: {
    const isMod = MKShortcuts.ShortcutUtil.isModifier(event.key, event.modifiers);
    const key = MKShortcuts.ShortcutUtil.keyToString(event.key, event.modifiers);

    if (isMod) {
      current = key;
      resultTimer.stop();
    } else {
      past.push(key);
      current = '';
      resultTimer.restart();
    }
    event.accepted = true;
  }

  Keys.onShortcutOverride: {
    event.accepted = true;
  }

  function startRecording() {
    past = [];
    current = '';
    focus = true;
  }

  function toSeqString(list) {
    return list.join(',');
  }

  function cancel() {
    past = [];
    current = '';
    focus = false;
  }
}
