/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import 'matrix-helpers.js' as Helpers

QtObject {
  property var room
  property var name: getName(room)

  function getName(r) {
    if (!r) {
      return '';
    }

    return Helpers.roomNameOrHeroes(
      r,
      r.heroEvents,
      sdkVars.userGivenNicknameMap.map,
      l10n,
    );
  }
}
