/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

import org.kde.kirigami 2.13 as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK
import '.' as Kazv
import 'matrix-helpers.js' as Helpers

ItemDelegate {
  id: readIndicator
  property var shouldShow: true
  property var model
  property var maxItems: 4
  property var avatarSize: Kirigami.Units.iconSizes.sizeForLabels || Kirigami.Units.iconSizes.small / 2
  property var actualItems: Math.min(model.count, maxItems)

  visible: shouldShow && readIndicator.model.count > 0

  contentItem: RowLayout {
    id: layout
    objectName: 'readIndicatorLayout'
    Repeater {
      model: readIndicator.actualItems

      Kazv.AvatarAdapter {
        objectName: `readIndicatorAvatar${index}`
        property var member: readIndicator.model.at(index)
        property var nameProvider: Kazv.UserNameProvider {
          user: member
        }
        Layout.preferredWidth: readIndicator.avatarSize
        Layout.preferredHeight: readIndicator.avatarSize
        mxcUri: member.avatarMxcUri
        name: nameProvider.name
      }
    }

    Label {
      objectName: 'moreUsersIndicator'
      visible: readIndicator.model.count > readIndicator.maxItems
      text: l10n.get('event-read-indicator-more', { rest: readIndicator.model.count - readIndicator.maxItems })
    }
  }

  Kirigami.OverlayDrawer {
    id: readIndicatorDrawer
    edge: Qt.BottomEdge
    modal: true
    Component.onCompleted: {
      if (MK.KazvUtil.kfQtMajorVersion === 6) {
        readIndicatorDrawer.parent = Overlay.overlay;
      }
    }
    contentItem: ColumnLayout {
      Label {
        text: l10n.get('event-read-indicator-list-title', { numUsers: readIndicator.model.count })
      }

      Repeater {
        model: readIndicator.model

        ItemDelegate {
          Layout.fillWidth: true
          property var member: readIndicator.model.at(index)
          property var nameProvider: Kazv.UserNameProvider {
            user: member
          }
          onPressed: {
            activateUserPage(member, room);
            readIndicatorDrawer.close();
          }
          contentItem: RowLayout {
            id: itemLayout
            Kazv.AvatarAdapter {
              objectName: `readIndicatorAvatar${index}`
              Layout.preferredWidth: readIndicator.avatarSize
              Layout.preferredHeight: readIndicator.avatarSize
              mxcUri: member.avatarMxcUri
              name: nameProvider.name
            }

            Label {
              text: nameProvider.name
              Layout.fillWidth: true
            }

            Label {
              Layout.alignment: Qt.AlignRight
              text: member.formattedTime
            }
          }
        }
      }
    }
  }

  onPressed: readIndicatorDrawer.open()
}
