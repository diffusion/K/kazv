/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import org.kde.kirigami as Kirigami
import '.' as Kazv

ListView {
  id: eventHistoryView
  property var history
  model: history
  delegate: Kazv.EventViewWrapper {
    event: history.at(index)
    width: ListView.view.width
    compactMode: true
    displayTime: true
  }
}
