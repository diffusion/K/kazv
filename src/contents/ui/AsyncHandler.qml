/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

QtObject {
  id: asyncHandler
  required property var trigger
  property var promise: null
  signal resolved(bool success, var data)

  property var conn: Connections {
    target: asyncHandler.promise

    function onResolved(success, data) {
      asyncHandler.resolved(success, data);
      asyncHandler.promise = null;
    }
  }

  function call() {
    promise = trigger();
  }
}
