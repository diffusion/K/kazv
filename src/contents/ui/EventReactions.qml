/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.kde.kirigami as Kirigami
import moe.kazv.mxc.kazv 0.0 as MK

Flow {
  required property var reactions
  Repeater {
    model: reactions
    delegate: RowLayout {
      id: reaction
      objectName: `reaction${index}`
      spacing: -1
      Kirigami.Theme.colorSet: Kirigami.Theme.Button
      required property int index
      readonly property var item: reactions.at(index)
      readonly property string key: item.key
      readonly property double commonHeight: Math.max(content.implicitHeight, count.implicitHeight)
      readonly property string selfReactEventId: getSelfReactEventId()

      function getSelfReactEventId() {
        for (let i = 0; i < item.count; ++i) {
          const e = item.at(i);
          if (e.sender === matrixSdk.userId) {
            return e.eventId;
          }
        }
        return '';
      }

      ItemDelegate {
        id: content
        objectName: 'reactionContent'
        Layout.preferredHeight: reaction.commonHeight
        rightInset: 0
        contentItem: Text {
          objectName: 'reactionContentText'
          textFormat: Qt.RichText
          horizontalAlignment: Qt.AlignHCenter
          verticalAlignment: Qt.AlignVCenter
          // See the comments in event-types/Reaction.qml
          text: '<span style="font-family: emoji, sans-serif;">' + MK.KazvUtil.escapeHtml(key) + '</span>'
          Accessible.description: l10n.get('event-react-button-description', { key })
          color: content.checked ? Kirigami.Theme.highlightedTextColor : Kirigami.Theme.textColor
        }
        checked: !!reaction.selfReactEventId
        onClicked: reaction.selfReactEventId ? deleteEventRequestedNoConfirm(reaction.selfReactEventId, /* isLocalEcho = */ false) : eventView.reactWith(key)
        background: Rectangle {
          topLeftRadius: Kirigami.Units.cornerRadius
          bottomLeftRadius: Kirigami.Units.cornerRadius
          border.color: Kirigami.Theme.highlightColor
          color: content.hovered
            ? Kirigami.Theme.hoverColor
            : content.checked
            ? Kirigami.Theme.highlightColor
            : 'transparent'
        }
      }

      ItemDelegate {
        id: count
        objectName: 'reactionCount'
        Layout.preferredHeight: reaction.commonHeight
        leftInset: 0
        contentItem: Text {
          horizontalAlignment: Qt.AlignHCenter
          verticalAlignment: Qt.AlignVCenter
          text: `${reaction.item.count}`
          color: Kirigami.Theme.textColor
        }
        background: Rectangle {
          topRightRadius: Kirigami.Units.cornerRadius
          bottomRightRadius: Kirigami.Units.cornerRadius
          border.color: Kirigami.Theme.highlightColor
          color: count.hovered
            ? Kirigami.Theme.hoverColor
            : 'transparent'
        }
      }
    }
  }
}
