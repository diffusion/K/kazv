/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick

import '.' as Kazv

QtObject {
  id: provider
  property var pack
  readonly property var name: getName(pack)
  readonly property var roomNameProvider: Kazv.RoomNameProvider {
    room: sdkVars.roomList.room(pack.roomId)
  }

  function getName(pack)
  {
    if (pack.packName) {
      return pack.packName;
    }

    if (pack.isAccountData) {
      return l10n.get('sticker-picker-user-stickers');
    } else if (pack.isState) {
      const roomName = pack === provider.pack
            ? roomNameProvider.name
            : roomNameProvider.getName(sdkVars.roomList.room(pack.roomId));
      if (pack.stateKey) {
        return l10n.get('sticker-picker-room-sticker-pack-name', {
          stateKey: pack.stateKey,
          room: roomName,
        });
      } else {
        return l10n.get('sticker-picker-room-default-sticker-pack-name', {
          room: roomName,
        });
      }
    }
  }
}
