/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

// This file is just to provide a fake NotificationAction in KF5
// so QML's naming resolution will not complain.

import QtQuick 2.15

QtObject {
  property var label
}
