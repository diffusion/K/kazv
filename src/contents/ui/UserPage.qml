/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

import org.kde.kirigami as Kirigami
import moe.kazv.mxc.kazv as MK

import '.' as Kazv
import 'device-mgmt' as KazvDM
import 'matrix-helpers.js' as Helpers

Kazv.ClosableScrollablePage {
  id: userPage
  property string userId: ''

  property var user: ({})
  property var nameProvider: Kazv.UserNameProvider {
    user: userPage.user
  }
  property var room
  property var roomNameProvider: Kazv.RoomNameProvider {
    room: userPage.room
  }
  property var roomName: roomNameProvider.name
  property var editingPowerLevel: false
  property var submittingPowerLevel: false
  property var powerLevelsLoaded: false
  property var userPowerLevel: room.userPowerLevel(userId)
  property var kickingUser: false
  property var canKickUser: !(user.membership === 'ban' || user.membership === 'leave')
  property var banningUser: false
  property var unbanningUser: false

  Connections {
    target: room
    function onPowerLevelsChanged() {
      userPage.userPowerLevel = room.userPowerLevel(userPage.userId);
    }
  }

  title: nameProvider.name

  property var ensureMemberEvent: Kazv.AsyncHandler {
    trigger: () => room.ensureStateEvent('m.room.member', userId)
  }

  property var ensurePowerLevels: Kazv.AsyncHandler {
    trigger: () => room.ensureStateEvent('m.room.power_levels', '')
    onResolved: (success, data) => {
      if (success) {
        userPage.powerLevelsLoaded = true;
      }
    }
  }

  property var updatingNameOverride: false
  property var updateNameOverride: Kazv.AsyncHandler {
    trigger: () => {
      userPage.updatingNameOverride = true;
      return sdkVars.userGivenNicknameMap.setAndUpload(
        userPage.userId, nameOverrideInput.text || null);
    }
    onResolved: (success, data) => {
      userPage.updatingNameOverride = false;
      if (!success) {
        showPassiveNotification(l10n.get('user-page-update-name-override-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }
  property bool updatingSelfName: false
  property var updateSelfName: Kazv.AsyncHandler {
    trigger: () => {
      userPage.updatingSelfName = true;
      return userPage.room.setSelfName(selfNameInput.text);
    }
    onResolved: (success, data) => {
      userPage.updatingSelfName = false;
      if (!success) {
        showPassiveNotification(l10n.get('user-page-update-self-name-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
    }
  }

  Component.onCompleted: {
    userPage.ensureMemberEvent.call();
    userPage.ensurePowerLevels.call();
  }

  property var setPowerLevel: Kazv.AsyncHandler {
    trigger: () => {
      userPage.submittingPowerLevel = true;
      return room.setUserPowerLevel(userPage.userId, parseInt(newPowerLevel.text));
    }
    onResolved: (success, data) => {
      if (!success) {
        showPassiveNotification(l10n.get('user-page-set-power-level-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        userPage.editingPowerLevel = false;
      }
      userPage.submittingPowerLevel = false;
    }
  }

  property var kickUser: Kazv.AsyncHandler {
    trigger: () => {
      userPage.kickingUser = true;
      return room.kickUser(userPage.userId, kickUserReasonInput.text);
    }
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('user-page-kick-user-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        kickUserReasonInput.text = '';
      }
      userPage.kickingUser = false;
    }
  }

  property var banUser: Kazv.AsyncHandler {
    trigger: () => {
      userPage.banningUser = true;
      return room.banUser(userPage.userId, banUserReasonInput.text);
    }
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('user-page-ban-user-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      } else {
        banUserReasonInput.text = '';
      }
      userPage.banningUser = false;
    }
  }

  property var unbanUser: Kazv.AsyncHandler {
    trigger: () => {
      userPage.unbanningUser = true;
      return room.unbanUser(userPage.userId);
    }
    onResolved: {
      if (!success) {
        showPassiveNotification(l10n.get('user-page-unban-user-failed-prompt', { errorCode: data.errorCode, errorMsg: data.error }));
      }
      userPage.unbanningUser = false;
    }
  }

  ColumnLayout {
    Kazv.AvatarAdapter {
      id: avatar
      objectName: 'avatar'
      Layout.alignment: Qt.AlignHCenter
      Layout.preferredHeight: Kirigami.Units.iconSizes.enormous
      Layout.preferredWidth: Kirigami.Units.iconSizes.enormous
      mxcUri: userPage.user.avatarMxcUri
      name: nameProvider.name
    }

    ColumnLayout {
      Layout.preferredWidth: this.parent.width
      Label {
        objectName: 'userNameLabel'
        Layout.alignment: Qt.AlignHCenter
        text: !!userPage.user.name ? userPage.user.name : userPage.userId
      }
      Label {
        objectName: 'userIdLabel'
        Layout.alignment: Qt.AlignHCenter
        font: Kirigami.Theme.smallFont
        visible: !!userPage.user.name
        text: userPage.userId
      }
    }

    RowLayout {
      visible: room.membership === MK.MatrixRoom.Join && userPage.userId === matrixSdk.userId
      TextField {
        id: selfNameInput
        objectName: 'selfNameInput'
        placeholderText: l10n.get('user-page-self-name-placeholder')
        Layout.fillWidth: true
        Accessible.name: l10n.get('user-page-self-name-prompt')
        enabled: !userPage.updatingSelfName
        text: userPage.user.name
      }

      Button {
        objectName: 'saveSelfNameButton'
        text: l10n.get('user-page-save-self-name-action')
        enabled: !userPage.updatingSelfName
        onClicked: userPage.updateSelfName.call()
      }
    }

    RowLayout {
      // Do not allow user to set a name override for themselves
      visible: userPage.userId !== matrixSdk.userId
      TextField {
        id: nameOverrideInput
        objectName: 'nameOverrideInput'
        placeholderText: l10n.get('user-page-overrided-name-placeholder')
        Layout.fillWidth: true
        enabled: !userPage.updatingNameOverride
        text: nameProvider.overridedName
      }

      Button {
        objectName: 'saveNameOverrideButton'
        text: l10n.get('user-page-save-name-override-action')
        enabled: !userPage.updatingNameOverride
        onClicked: updateNameOverride.call()
      }
    }

    RowLayout {
      visible: !userPage.editingPowerLevel
      Label {
        objectName: 'powerLevelLabel'
        Layout.fillWidth: true
        text: l10n.get('user-page-power-level', { powerLevel: userPage.userPowerLevel })
      }

      Button {
        objectName: 'editPowerLevelButton'
        text: l10n.get('user-page-edit-power-level-action')
        enabled: userPage.powerLevelsLoaded
        onClicked: {
          userPage.editingPowerLevel = true;
        }
      }
    }

    RowLayout {
      visible: userPage.editingPowerLevel
      TextField {
        objectName: 'newPowerLevelInput'
        id: newPowerLevel
        Layout.fillWidth: true
        text: `${userPage.userPowerLevel}`
        readOnly: userPage.submittingPowerLevel
      }

      Button {
        objectName: 'savePowerLevelButton'
        text: l10n.get('user-page-save-power-level-action')
        onClicked: {
          setPowerLevel.call();
        }
        enabled: !userPage.submittingPowerLevel
      }

      Button {
        objectName: 'discardPowerLevelButton'
        text: l10n.get('user-page-discard-power-level-action')
        enabled: !userPage.submittingPowerLevel
        onClicked: {
          userPage.editingPowerLevel = false;
          newPowerLevel.text = `${userPage.userPowerLevel}`;
        }
      }
    }

    ColumnLayout {
      Kirigami.PromptDialog {
        id: kickUserReasonDialog
        objectName: 'kickUserReasonDialog'
        title: l10n.get('user-page-kick-user-confirm-dialog-title')
        standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel
        onAccepted: {
          userPage.kickUser.call()
        }

        ColumnLayout {
          Label {
            Layout.fillWidth: true
            text: l10n.get('user-page-kick-user-confirm-dialog-content', {
              userId: userPage.user.userId,
              name: nameProvider.name,
              roomName: userPage.roomName,
            })
            wrapMode: Text.Wrap
          }

          ColumnLayout {
            Label {
              Layout.fillWidth: true
              text: l10n.get('user-page-kick-user-confirm-dialog-content', {
                userId: userPage.user.userId,
                name: userPage.user.name,
                roomName: userPage.roomName,
              })
              wrapMode: Text.Wrap
            }

            Kirigami.FormLayout {
              Layout.fillWidth: true
              TextField {
                Layout.fillWidth: true
                id: kickUserReasonInput
                objectName: 'kickUserReasonInput'
                readOnly: userPage.kickingUser
                Kirigami.FormData.label: l10n.get('user-page-kick-user-reason-prompt')
              }
            }
          }
        }
      }
      Button {
        objectName: 'kickUserButton'
        icon.name: 'im-kick-user'
        Layout.fillWidth: true
        enabled: !userPage.kickingUser
        text: l10n.get('user-page-kick-user-action')
        visible: userPage.canKickUser
        onClicked: {
          kickUserReasonDialog.open()
        }
      }
    }

    RowLayout {
      Kirigami.PromptDialog {
        id: banUserReasonDialog
        objectName: 'banUserReasonDialog'
        title: l10n.get('user-page-ban-user-confirm-dialog-title')
        standardButtons: Kirigami.Dialog.Ok | Kirigami.Dialog.Cancel
        onAccepted: {
          userPage.banUser.call()
        }

        ColumnLayout {
          Label {
            Layout.fillWidth: true
            text: l10n.get('user-page-ban-user-confirm-dialog-content', {
              userId: userPage.user.userId,
              name: nameProvider.name,
              roomName: userPage.roomName,
            })
            wrapMode: Text.Wrap
          }

          Kirigami.FormLayout {
            Layout.fillWidth: true
            TextField {
              Layout.fillWidth: true
              id: banUserReasonInput
              objectName: 'banUserReasonInput'
              readOnly: userPage.banningUser
              Kirigami.FormData.label: l10n.get('user-page-ban-user-reason-prompt')
            }
          }
        }
      }

      Button {
        objectName: 'banUserButton'
        Layout.fillWidth: true
        icon.name: 'im-ban-kick-user'
        enabled: !userPage.banningUser
        text: l10n.get('user-page-ban-user-action')
        visible: !unbanUserButton.visible
        onClicked: {
          banUserReasonDialog.open()
        }
      }

      Button {
        id: unbanUserButton
        objectName: 'unbanUserButton'
        Layout.fillWidth: true
        icon.name: 'im-user-online'
        enabled: !userPage.unbanningUser
        text: l10n.get('user-page-unban-user-action')
        visible: user.membership === "ban"
        onClicked: {
          userPage.unbanUser.call()
        }
      }
    }

    KazvDM.DeviceList {
      Layout.fillWidth: true
      Layout.fillHeight: true
      Layout.minimumHeight: childrenRect.height
      userId: userPage.userId
      devices: matrixSdk.devicesOfUser(userId)
    }
  }
}
