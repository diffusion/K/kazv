/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once
#include <kazv-defs.hpp>

#include <QObject>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QString>
#include <QJsonObject>

struct L10nProviderPrivate;
class L10nProvider : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    QScopedPointer<L10nProviderPrivate> m_d;

public:
    explicit L10nProvider(QObject *parent = 0);
    ~L10nProvider() override;

    Q_INVOKABLE QStringList availableLocaleCodes();

    Q_INVOKABLE QJsonObject availableLocaleToNameMap();

    Q_INVOKABLE QJsonObject getFtlData(QStringList desiredLocales);
};
