/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <cmath>

#include <lager/lenses/optional.hpp>

#include <cursorutil.hpp>
#include "helper.hpp"
#include "matrix-room-member-list-model.hpp"
#include "matrix-room-member.hpp"

using namespace Kazv;

MatrixRoomMemberListModel::MatrixRoomMemberListModel(lager::reader<Kazv::EventList> members, QString filter, lager::reader<Kazv::Event> userGivenNicknameEvent, QObject *parent)
    : KazvAbstractListModel(parent)
    , m_filter(lager::make_state(filter.toStdString(), lager::automatic_tag{}))
    , m_members(lager::with(
        members,
        m_filter,
        userGivenNicknameEvent.map(&Event::content)
    ).map([](const auto &members, const auto &filter, const auto &map) {
        if (filter.empty()) {
            return members;
        }

        return intoImmer(EventList{}, zug::filter([filter, map=map.get()](const auto &e) {
            auto userId = e.stateKey();
            if (userId.find(filter) != std::string::npos) {
                return true;
            }
            // user given nickname match
            auto content = e.content().get();
            if (map.contains(userId) && map[userId].is_string()) {
                auto name = map[userId].template get<std::string>();
                if (name.find(filter) != std::string::npos) {
                    return true;
                }
            }
            if (content.contains("displayname")
                && content["displayname"].is_string()) {
                auto name = content["displayname"].template get<std::string>();
                return name.find(filter) != std::string::npos;
            }
            return false;
        }), members);
    }))
    , LAGER_QT(filter)(m_filter.xform(strToQt, qStringToStd))
{
    initCountCursor(m_members.map([](const auto &m) -> int { return m.size(); }));
}

MatrixRoomMemberListModel::~MatrixRoomMemberListModel() = default;

MatrixRoomMember *MatrixRoomMemberListModel::at(int index) const
{
    return new MatrixRoomMember(m_members[index][lager::lenses::or_default]);
}
