/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2021-2023 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>

#include <immer/config.hpp> // https://github.com/arximboldi/immer/issues/168

#include <QApplication>
#include <QByteArray>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QUrl>
#include <QIcon>
#include <QCommandLineParser>
#include <QQuickStyle>
#include <KAboutData>
#if KAZV_LINK_BREEZE_ICONS
#include <BreezeIcons>
#endif
#include "meta-types.hpp"
#include "kazv-platform.hpp"
#include "kazv-path-config.hpp"
#include "kazv-version.hpp"
#include "kazv-log.hpp"

using namespace Qt::Literals::StringLiterals;

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName(u"project-kazv"_s);
    QCoreApplication::setOrganizationDomain(u"mxc.kazv.moe"_s);
    QCoreApplication::setApplicationName(u"kazv"_s);
    QGuiApplication::setDesktopFileName(u"moe.kazv.mxc.kazv.desktop"_s);

    KAboutData aboutData;
    aboutData
        .setComponentName(u"kazv"_s)
        .setVersion(QByteArray::fromStdString(kazvVersionString()))
        .setOrganizationDomain(QByteArray("mxc.kazv.moe"))
        .setDesktopFileName(u"moe.kazv.mxc.kazv"_s)
        .setBugAddress(QByteArray("https://lily-is.land/kazv/kazv/-/issues"))
        .setHomepage(u"https://kazv.chat"_s);
    KAboutData::setApplicationData(aboutData);

#if KAZV_IS_WINDOWS
    if (qEnvironmentVariableIsEmpty("QT_QUICK_CONTROLS_STYLE")) {
        QQuickStyle::setStyle(QStringLiteral("org.kde.desktop"));
    }
#endif

    QQmlApplicationEngine engine;

#if KAZV_LINK_BREEZE_ICONS
    BreezeIcons::initIcons();
#endif

    QString iconThemeToSet;
#if KAZV_IS_WINDOWS
    iconThemeToSet = QStringLiteral("breeze");
#endif
    QCommandLineParser parser;
    QCommandLineOption iconThemeOption(u"i"_s, u"Icon theme"_s, u"theme"_s);
    parser.addOption(iconThemeOption);
    parser.process(app);
    if (!parser.value(iconThemeOption).isEmpty()) {
        iconThemeToSet = parser.value(iconThemeOption);
    }

    KazvMetaTypeRegistration registration;
    Q_UNUSED(registration);

#if KAZV_IS_WINDOWS
    // On Windows the default search path is only in qrc
    QStringList iconThemePaths;
    iconThemePaths << (appDir() + QStringLiteral("/data/icons"));

    QIcon::setThemeSearchPaths(iconThemePaths);
#endif

    if (!iconThemeToSet.isEmpty()) {
        QIcon::setThemeName(iconThemeToSet);
    }
    engine.loadFromModule(u"moe.kazv.mxc.kazvqml"_s, u"Main"_s);

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    return app.exec();
}
