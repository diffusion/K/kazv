/*
 * This file is part of kazv.
 * SPDX-FileCopyrightText: 2020-2024 tusooa <tusooa@kazv.moe>
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <kazv-defs.hpp>
#include "shortcut-util.hpp"
#include <QKeySequence>

using namespace Qt::Literals::StringLiterals;

// https://stackoverflow.com/questions/64826271/how-to-use-qkeysequence-or-qkeysequenceedit-from-qml
static bool isKeyUnknown(int key)
{
    // weird key codes that appear when modifiers
    // are pressed without accompanying standard keys
    constexpr int NO_KEY_LOW = 16777248;
    constexpr int NO_KEY_HIGH = 16777251;
    if (NO_KEY_LOW <= key && key <= NO_KEY_HIGH) {
        return true;
    }

    if (key == Qt::Key_unknown) {
        return true;
    }

    return false;
}

bool ShortcutUtil::isModifier(int key, int /* modifiers */) const
{
    return isKeyUnknown(key);
}

QString ShortcutUtil::keyToString(int key, int modifiers) const
{
    if (!isKeyUnknown(key)) {
        return QKeySequence(key | modifiers).toString();
    } else {
        // Change to "Ctrl+[garbage]" to "Ctrl+_"
        QString modifierOnlyString = QKeySequence(Qt::Key_Underscore | modifiers).toString();

        // Change "Ctrl+_" to "Ctrl+..."
        modifierOnlyString.replace('_'_L1, u"..."_s);
        return modifierOnlyString;
    }
}
